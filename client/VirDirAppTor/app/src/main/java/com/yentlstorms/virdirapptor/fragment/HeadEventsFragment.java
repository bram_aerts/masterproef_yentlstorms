package com.yentlstorms.virdirapptor.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yentlstorms.virdirapptor.R;

public class HeadEventsFragment extends Fragment {

    public HeadEventsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HeadEventsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HeadEventsFragment newInstance() {
        HeadEventsFragment fragment = new HeadEventsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_head_events, container, false);
    }

}
