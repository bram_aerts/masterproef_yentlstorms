package com.yentlstorms.virdirapptor.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;


import com.yentlstorms.virdirapptor.R;
import com.yentlstorms.virdirapptor.service.EventInfoService;
import com.yentlstorms.virdirapptor.service.UserInfoService;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HeadHomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HeadHomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HeadHomeFragment extends Fragment {

    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    public static int INT_ITEMS = 3;

    private OnFragmentInteractionListener mListener;

   /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HeadHomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HeadHomeFragment newInstance() {
        HeadHomeFragment fragment = new HeadHomeFragment();
        return fragment;
    }

    public HeadHomeFragment() {
        // Required empty public constructor
    }

    private void getUserInfo() {
        // TODO: change ID 1 to the actual logged-in person's id
        UserInfoService.startActionGetUserInfo(getContext(), 1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v =  inflater.inflate(R.layout.fragment_head_home, null);
        mTabLayout = (TabLayout) v.findViewById(R.id.fragment_home_tabs);
        mViewPager = (ViewPager) v.findViewById(R.id.fragment_viewpager);

        mViewPager.setAdapter(new MyAdapter(getChildFragmentManager()));
        mViewPager.setOffscreenPageLimit(2);
        mTabLayout.setupWithViewPager(mViewPager);

    /*
        mTabLayout.post(new Runnable() {
            @Override
            public void run() {
                mTabLayout.setupWithViewPager(mViewPager);
            }
        });*/
        // TODO: Only call getUserInfo(); when server has new data. (using GCM) (now every time new homefragment is made)
        if (savedInstanceState == null) {
            getUserInfo();
            //EventInfoService.startActionGetEventInfo(getContext(), 1);
            EventInfoService.startActionGetEventInfoForPerson(getContext(), 1);
        }
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            //mViewPager.setCurrentItem(savedInstanceState.getInt("currentItem"), true);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("currentItem", mViewPager.getCurrentItem());
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    class MyAdapter extends FragmentStatePagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */
        @Override
        public Fragment getItem(int position)
        {
            switch (position){
                case 0 : return UserFragment.newInstance();
                case 1 : return FriendsFragment.newInstance();
                case 2 : return EventsFragment.newInstance();
            }
            return null;
        }

        @Override
        public int getCount() {
            return INT_ITEMS;
        }

        /**
         * This method returns the title of the tab according to the position.
         */
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0 :
                    return "ME";
                case 1 :
                    return "FOLLOWING";
                case 2 :
                    return "EVENTS";
            }
            return null;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.v("HeadHomeFragment", "onActivityResult in HeadHomeFragment");
        super.onActivityResult(requestCode, resultCode, data);

        List<Fragment> fragments = getChildFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null) {
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }
}
