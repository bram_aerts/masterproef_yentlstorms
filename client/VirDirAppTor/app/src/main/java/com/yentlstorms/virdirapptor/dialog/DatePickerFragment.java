package com.yentlstorms.virdirapptor.dialog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.yentlstorms.virdirapptor.Constants;

/**
 * Created by Yentl on 19/11/2015.
 */
public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    public static DatePickerFragment newInstance() {
        DatePickerFragment frag = new DatePickerFragment();
        return frag;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int day, month, year;
        Bundle dateBundle = getArguments().getBundle("currentValue");
        if (dateBundle == null) {
            day = 15;
            month = 6;
            year = 2000;
        } else {
            day = dateBundle.getInt(Constants.BUNDLE_KEY_DATE_YEAR, 2000);
            month = dateBundle.getInt(Constants.BUNDLE_KEY_DATE_MONTH, 5);
            year = dateBundle.getInt(Constants.BUNDLE_KEY_DATE_DAY, 15);
        }
        DatePickerDialog dpDialog = new DatePickerDialog(
                getActivity(),
                this,
                year,
                month,
                day
        );
        dpDialog.setTitle(getArguments().getString("title"));


        return dpDialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        //TODO: clean this up, change to bundle + in personfragment as well!
        Intent intent = new Intent();
        Bundle resultBundle = new Bundle();
        resultBundle.putInt(Constants.BUNDLE_KEY_DATE_YEAR, year);
        resultBundle.putInt(Constants.BUNDLE_KEY_DATE_MONTH, monthOfYear);
        resultBundle.putInt(Constants.BUNDLE_KEY_DATE_DAY, dayOfMonth);
        intent.putExtras(resultBundle);
        getTargetFragment().onActivityResult(getTargetRequestCode(), 1, intent);
    }
}