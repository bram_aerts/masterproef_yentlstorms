package com.yentlstorms.virdirapptor.adapter;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.yentlstorms.virdirapptor.R;
import com.yentlstorms.virdirapptor.Utility;
import com.yentlstorms.virdirapptor.fragment.EventsFragment;
import com.yentlstorms.virdirapptor.service.EventUtility;

import java.io.File;

/**
 * Created by Yentl on 29/02/2016.
 */
public class EventAdapter extends CursorAdapter {

    public static class ViewHolder {
        public final ImageView iconView;
        public final TextView eventnameView;
        public final TextView startdateView;

        public ViewHolder(View view) {
            iconView = (ImageView) view.findViewById(R.id.events_list_item_icon);
            eventnameView = (TextView) view.findViewById(R.id.events_list_item_eventname);
            startdateView = (TextView) view.findViewById(R.id.events_list_item_start_end_date);
        }
    }

    public EventAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        View view = LayoutInflater.from(context).inflate(R.layout.events_list_item_small, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);

        return view;
    }

    public void bindView(View view, Context context, Cursor data) {

        if (data.getCount() < 1) {
            CharSequence text = "Could not find your userdata in database";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            return;
        }

        ViewHolder viewHolder = (ViewHolder) view.getTag();

        File eventPicture = new File(Utility.getEventPicturesDirectory(context),
                data.getString(EventsFragment.COL_EVENTS_PICTURE_URL));
        Log.v("PersonAdapter.java", " "+eventPicture.getAbsolutePath());
        if ( eventPicture.exists() ) {
            Log.v("PersonAdapter.java", "exists " + eventPicture.getAbsolutePath());
            viewHolder.iconView.setImageURI(Uri.fromFile(eventPicture));
        } else {
            viewHolder.iconView.setImageResource(R.drawable.user);
        }
        Log.v("PersonAdapter.java", data.getString(EventsFragment.COL_EVENTS_ID) + " --- " +
                        data.getString(EventsFragment.COL_EVENTS_EVENTNAME) + " --- " +
                        data.getString(EventsFragment.COL_EVENTS_STARTDATE) + " --- " +
                        data.getString(EventsFragment.COL_EVENTS_ENDDATE) + " --- " +
                        data.getString(EventsFragment.COL_EVENTS_PICTURE_URL)
        );

        // Read data from cursor and display it on the correct Views.
        String eventname = data.getString(EventsFragment.COL_EVENTS_EVENTNAME);
        viewHolder.eventnameView.setText(eventname);

        String startdate = data.getString(EventsFragment.COL_EVENTS_STARTDATE);
        String enddate = data.getString(EventsFragment.COL_EVENTS_ENDDATE);

        String displayDate = EventUtility.datesToEventListString(startdate, enddate);
        viewHolder.startdateView.setText(displayDate);

    }
}
