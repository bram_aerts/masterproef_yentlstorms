package com.yentlstorms.virdirapptor.bottomsheet;

import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yentlstorms.virdirapptor.R;
import com.yentlstorms.virdirapptor.Utility;
import com.yentlstorms.virdirapptor.data.DataContract;
import com.yentlstorms.virdirapptor.fragment.UserFragment;


import java.io.File;

/**
 * Created by Yentl on 5/03/2016.
 */
public class PersonBottomSheetFragment extends BottomSheetDialogFragment
        implements LoaderManager.LoaderCallbacks<Cursor> {

    public final int PERSONBOTTOMSHEETFRAGMENT_LOADER = 18567; // TODO

    ImageView imageView;
    TextView usernameView;
    TextView personnameView;
    TextView dobView;
    TextView locationCountryView;

    public PersonBottomSheetFragment() {
        ;
    }

    public static PersonBottomSheetFragment newInstance(int personid) {
        PersonBottomSheetFragment frag = new PersonBottomSheetFragment();
        Bundle argsBundle = new Bundle();
        argsBundle.putInt("personid", personid);
        frag.setArguments(argsBundle);
        return frag;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_person_bottom_sheet, null);
        imageView = (ImageView) view.findViewById(R.id.person_bottom_sheet_image);
        usernameView = (TextView) view.findViewById(R.id.person_bottom_sheet_username);
        personnameView = (TextView) view.findViewById(R.id.person_bottom_sheet_personname);
        dobView = (TextView) view.findViewById(R.id.fragment_person_dob);
        locationCountryView = (TextView) view.findViewById(R.id.fragment_person_location_country);

        getLoaderManager().initLoader(PERSONBOTTOMSHEETFRAGMENT_LOADER, null, this);

        return view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {

        Uri personsWithIDUri = DataContract.PersonsEntry.buildPersonsUri(getArguments().getInt("personid"));

        return new CursorLoader(getActivity(),
                personsWithIDUri,
                UserFragment.PERSONFRAGMENT_COLUMNS,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if (data == null || !data.moveToFirst()) {
            // null or empty cursor
            return;
        }

        File mCroppedPictureFile = new File(Utility.getProfilePicturesDirectory(getContext()),
                data.getString(UserFragment.COL_PERSONS_PICTURE_URL));
        imageView.setImageURI(Uri.fromFile(mCroppedPictureFile));
        usernameView.setText(data.getString(UserFragment.COL_PERSONS_USERNAME));
        personnameView.setText(data.getString(UserFragment.COL_PERSONS_PERSONNAME));
        dobView.setText(data.getString(UserFragment.COL_PERSONS_DOB));
        locationCountryView.setText(data.getString(UserFragment.COL_PERSONS_LOCATION_COUNTRY));
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.v("PersonBottomSheetFrag", "dismissed");
    }
}
