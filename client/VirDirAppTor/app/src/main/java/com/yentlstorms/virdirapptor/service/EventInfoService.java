package com.yentlstorms.virdirapptor.service;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.yentlstorms.virdirapptor.Constants;
import com.yentlstorms.virdirapptor.Utility;
import com.yentlstorms.virdirapptor.data.DataContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class EventInfoService extends IntentService {
    private static final String LOG_TAG = EventInfoService.class.getSimpleName();

    private static final String ACTION_GET_EVENT_INFO =
            "com.yentlstorms.virdirapptor.service.action.geteventinfo";
    private static final String ACTION_GET_EVENT_INFO_FOR_PERSON =
            "";
    private static final String ACTION_UPDATE_EVENT_INFO =
            "com.yentlstorms.virdirapptor.service.action.updateventinfo";
    private static final String ACTION_CREATE_NEW_EVENT =
            "com.yentlstorms.virdirapptor.service.action.createnewevent";

    public static final String PERSONID_QUERY_EXTRA = "pqe";
    public static final String EVENTID_QUERY_EXTRA = "eqe";
    public static final String EVENT_DATA_QUERY_EXTRA = "uqe";

    public EventInfoService() {
        super("EventInfoService");
    }

   public static void startActionGetEventInfo(Context context, int eventid) {
        Intent intent = new Intent(context, EventInfoService.class);
        intent.setAction(ACTION_GET_EVENT_INFO);
        intent.putExtra(EVENTID_QUERY_EXTRA, String.valueOf(eventid));
        context.startService(intent);
    }

    public static void startActionGetEventInfoForPerson(Context context, int personid) {
        Log.v(LOG_TAG, "startActionGetEventInfoForPerson started");
        Intent intent = new Intent(context, EventInfoService.class);
        intent.setAction(ACTION_GET_EVENT_INFO_FOR_PERSON);
        intent.putExtra(PERSONID_QUERY_EXTRA, String.valueOf(personid));
        context.startService(intent);
    }

    public static void startActionUpdateEventInfo(Context context, Bundle eventData) {
        Intent intent = new Intent(context, EventInfoService.class);
        intent.setAction(ACTION_UPDATE_EVENT_INFO);
        intent.putExtra(EVENT_DATA_QUERY_EXTRA, eventData);
        context.startService(intent);
    }

    public static void startActionCreateNewEvent(Context context, Bundle eventData) {
        Intent intent = new Intent(context, EventInfoService.class);
        intent.setAction(ACTION_CREATE_NEW_EVENT);
        intent.putExtra(EVENT_DATA_QUERY_EXTRA, eventData);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GET_EVENT_INFO.equals(action)) {
                handleGetEventInfo(intent.getStringExtra(EVENTID_QUERY_EXTRA));
            } else if (ACTION_GET_EVENT_INFO_FOR_PERSON.equals(action)) {
                handleGetEventInfoForPerson(intent.getStringExtra(PERSONID_QUERY_EXTRA));
            } else if (ACTION_UPDATE_EVENT_INFO.equals(action)) {
                handleUpdateEventInfo();
            } else if (ACTION_CREATE_NEW_EVENT.equals(action)) {
                handleCreateNewEvent();
            }
        }
    }

    private void handleGetEventInfo(String eventid) {
        Log.v(LOG_TAG, "Started handleGetEventInfo with eventid = " + eventid);

        try {
            // Construct the URL for the Server request
            final String PERSONS_BASE_URL =
                    Constants.SERVER_BASE_URL + "Event?";
            final String ACTION_PARAM = "action";
            final String ACTION_VALUE = "getEventInfo";
            final String EVENTID_PARAM = "eventid";

            Uri builtUri = Uri.parse(PERSONS_BASE_URL).buildUpon()
                    .appendQueryParameter(ACTION_PARAM, ACTION_VALUE)
                    .appendQueryParameter(EVENTID_PARAM, eventid)
                    .build();

            String responseJSONString = Utility.getJSONStringFromServer(builtUri);
            Log.v(LOG_TAG, "JSON string:" + responseJSONString);

            JSONObject responseJSON = new JSONObject(responseJSONString);
            extractEventDataFromJSON(responseJSON.getJSONObject(Constants.JSON_KEY_EVENT), 0);

            Log.d(LOG_TAG, "EventInfoService Complete (GetEventInfo).");
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
        return;
    }

    private void handleGetEventInfoForPerson(String personid) {
        Log.v(LOG_TAG, "Started handleGetEventInfoForPerson with person id = " + personid);
        try {
            // Construct the URL for the Server request
            final String PERSONS_BASE_URL =
                    Constants.SERVER_BASE_URL + "Event?";
            final String ACTION_PARAM = "action";
            final String ACTION_VALUE = "getEventInfo";
            final String PERSONID_PARAM = "personid";

            Uri builtUri = Uri.parse(PERSONS_BASE_URL).buildUpon()
                    .appendQueryParameter(ACTION_PARAM, ACTION_VALUE)
                    .appendQueryParameter(PERSONID_PARAM, personid)
                    .build();

            Log.v(LOG_TAG, "Uri used = " + builtUri.toString());
            String responseJSONString = Utility.getJSONStringFromServer(builtUri);
            Log.v(LOG_TAG, "JSON string:" + responseJSONString);

            if ( responseJSONString == null ) {
                return;
            }
            JSONObject responseJSON = new JSONObject(responseJSONString);
            JSONArray eventsArray = responseJSON.getJSONArray(Constants.JSON_KEY_EVENTS);
            for ( int i = 0; i < eventsArray.length(); i++ ) {
                extractEventDataFromJSON(eventsArray.getJSONObject(i).getJSONObject(Constants.JSON_KEY_EVENT),
                        Integer.parseInt(personid));
            }
            Log.d(LOG_TAG, "EventInfoService Complete (GetEventInfo).");
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
        return;
    }

    // attenderID is used when we requested the events a specific person goes to (the attender).
    // set to 0 otherwise.
    private void extractEventDataFromJSON(JSONObject eventJSON, int attenderID)
            throws JSONException {

        final String PROFILE_PICTURES_BASE_URL =
                Constants.SERVER_BASE_URL + "file" + File.separator + "ProfilePictures";
        final String EVENT_PICTURES_BASE_URL =
                Constants.SERVER_BASE_URL + "file" + File.separator + "EventPictures";
        Uri insertedUri;
        try {
            //JSONObject eventJSON = new JSONObject(resultJSONString);
            JSONObject creatorJSON = eventJSON.getJSONObject(Constants.JSON_KEY_CREATOR);
            JSONArray labelsJSONArray = eventJSON.getJSONArray(Constants.JSON_KEY_LABELS);
            Log.v(LOG_TAG, creatorJSON.toString());

            //**************** CREATOR START: First add the event's creator, as this is used as a foreign key in event.

            int personid = creatorJSON.getInt(Constants.JSON_KEY_PERSONID);
            String username = creatorJSON.getString(Constants.JSON_KEY_USERNAME);
            String personname = creatorJSON.getString(Constants.JSON_KEY_PERSONNAME);
            String dob = creatorJSON.getString(Constants.JSON_KEY_DOB);
            String locationcountry = creatorJSON.getString(Constants.JSON_KEY_LOCATIONCOUNTRY);
            String pictureurl = creatorJSON.getString(Constants.JSON_KEY_PICTUREURL);
            String lastchanged = creatorJSON.getString(Constants.JSON_KEY_LASTCHANGED);

            ContentValues creatorValues = new ContentValues();
            creatorValues.put(DataContract.PersonsEntry._ID, personid);
            creatorValues.put(DataContract.PersonsEntry.COLUMN_USERNAME, username);
            creatorValues.put(DataContract.PersonsEntry.COLUMN_PERSONNAME, personname);
            creatorValues.put(DataContract.PersonsEntry.COLUMN_DOB, dob);
            creatorValues.put(DataContract.PersonsEntry.COLUMN_LOCATION_STREET, "");
            creatorValues.put(DataContract.PersonsEntry.COLUMN_LOCATION_NUMBER, "");
            creatorValues.put(DataContract.PersonsEntry.COLUMN_LOCATION_POSTAL, "");
            creatorValues.put(DataContract.PersonsEntry.COLUMN_LOCATION_TOWN, "");
            creatorValues.put(DataContract.PersonsEntry.COLUMN_LOCATION_COUNTRY, locationcountry);
            creatorValues.put(DataContract.PersonsEntry.COLUMN_EMAIL, "");
            creatorValues.put(DataContract.PersonsEntry.COLUMN_LAST_CHANGED, lastchanged);

            // download the creator's profile picture
            try {
                Log.v(LOG_TAG, "download creatorimage (start)");
                URL source = new URL(PROFILE_PICTURES_BASE_URL + File.separator + pictureurl);
                Log.v(LOG_TAG, "source = "+source.toString());
                File destination = new File(Utility.getProfilePicturesDirectory(getApplicationContext()), pictureurl);
                Log.v(LOG_TAG, "destination = " + destination.getAbsolutePath());
                Log.v(LOG_TAG, source.toString());
                if (Utility.downloadImageFromServer(source, destination)) {
                    creatorValues.put(DataContract.PersonsEntry.COLUMN_PICTURE_URL, destination.getName());
                } else {
                    creatorValues.put(DataContract.PersonsEntry.COLUMN_PICTURE_URL, "");
                }
            } catch (MalformedURLException mfURLExc) {
                Log.v(LOG_TAG, mfURLExc.toString());
                creatorValues.put(DataContract.PersonsEntry.COLUMN_PICTURE_URL, "");
            }

            // TODO: check if personid == logged in user id --> don't replace, but update!
            if (personid != 1) {
                // and insert the creator into the database
                insertedUri = this.getContentResolver().insert(
                        DataContract.PersonsEntry.CONTENT_URI,
                        creatorValues
                );
            }
            //**************** CREATOR END

            //**************** EVENT START
            int eventid = eventJSON.getInt(Constants.JSON_KEY_EVENTID);
            String eventname = eventJSON.getString(Constants.JSON_KEY_EVENTNAME);
            String startdate = eventJSON.getString(Constants.JSON_KEY_STARTDATE);
            String enddate = eventJSON.getString(Constants.JSON_KEY_ENDDATE);
            String locationstreet = eventJSON.getString(Constants.JSON_KEY_LOCATIONSTREET);
            int locationnumber = eventJSON.getInt(Constants.JSON_KEY_LOCATIONNUMBER);
            int locationpostal = eventJSON.getInt(Constants.JSON_KEY_LOCATIONPOSTAL);
            String locationtown = eventJSON.getString(Constants.JSON_KEY_LOCATIONTOWN);
            locationcountry = eventJSON.getString(Constants.JSON_KEY_LOCATIONCOUNTRY);
            pictureurl = eventJSON.getString(Constants.JSON_KEY_PICTUREURL);
            String description = eventJSON.getString(Constants.JSON_KEY_DESCRIPTION);
            lastchanged = eventJSON.getString(Constants.JSON_KEY_LASTCHANGED);

            ContentValues eventValues = new ContentValues();
            eventValues.put(DataContract.EventsEntry._ID, eventid);
            eventValues.put(DataContract.EventsEntry.COLUMN_EVENTNAME, eventname);
            eventValues.put(DataContract.EventsEntry.COLUMN_STARTDATE, startdate);
            eventValues.put(DataContract.EventsEntry.COLUMN_ENDDATE, enddate);
            eventValues.put(DataContract.EventsEntry.COLUMN_LOCATION_STREET, locationstreet);
            eventValues.put(DataContract.EventsEntry.COLUMN_LOCATION_NUMBER, locationnumber);
            eventValues.put(DataContract.EventsEntry.COLUMN_LOCATION_POSTAL, locationpostal);
            eventValues.put(DataContract.EventsEntry.COLUMN_LOCATION_TOWN, locationtown);
            eventValues.put(DataContract.EventsEntry.COLUMN_LOCATION_COUNTRY, locationcountry);
            eventValues.put(DataContract.EventsEntry.COLUMN_DESCRIPTION, description);
            eventValues.put(DataContract.EventsEntry.COLUMN_CREATOR_ID, personid);
            eventValues.put(DataContract.EventsEntry.COLUMN_LAST_CHANGED, lastchanged);

            try {
                Log.v(LOG_TAG, "download eventimage (start)");
                URL source = new URL(EVENT_PICTURES_BASE_URL + File.separator + pictureurl);
                Log.v(LOG_TAG, "source = "+source.toString());
                File destination = new File(Utility.getEventPicturesDirectory(getApplicationContext()), pictureurl);
                Log.v(LOG_TAG, "destination = " + destination.getAbsolutePath());
                Log.v(LOG_TAG, source.toString());
                if (Utility.downloadImageFromServer(source, destination)) {
                    eventValues.put(DataContract.PersonsEntry.COLUMN_PICTURE_URL, destination.getName());
                } else {
                    eventValues.put(DataContract.PersonsEntry.COLUMN_PICTURE_URL, "");
                }
            } catch (MalformedURLException mfURLExc) {
                Log.v(LOG_TAG, mfURLExc.toString());
                eventValues.put(DataContract.PersonsEntry.COLUMN_PICTURE_URL, "");
            }
            // and insert the event into the database
            insertedUri = this.getContentResolver().insert(
                    DataContract.EventsEntry.CONTENT_URI,
                    eventValues
            );
            //**************** EVENT END

            //**************** PERSONS_EVENTS START
            if ( attenderID >  0) {
                ContentValues personsEventsValues = new ContentValues();
                personsEventsValues.put(DataContract.PersonsEventsEntry._ID, attenderID);
                personsEventsValues.put(DataContract.PersonsEventsEntry.COLUMN_EVENTID, eventid);

                insertedUri = this.getContentResolver().insert(
                        DataContract.PersonsEventsEntry.CONTENT_URI,
                        personsEventsValues
                );
            }
            //**************** PERSONS_EVENTS END

            //**************** LABELS START
            Vector<ContentValues> labelsCVVector = new Vector<ContentValues>(labelsJSONArray.length());
            Vector<ContentValues> eventLabelsCVVector = new Vector<ContentValues>(labelsJSONArray.length());
            for( int i = 0; i < labelsJSONArray.length(); i++ ) {
                JSONObject labelElement = labelsJSONArray.getJSONObject(i);
                ContentValues labelValues = new ContentValues();
                ContentValues eventLabelsValues = new ContentValues();
                int labelid = labelElement.getInt(Constants.JSON_KEY_LABELID);
                String labelName = labelElement.getString(Constants.JSON_KEY_LABELNAME);
                // Add to table that holds the labels
                labelValues.put(DataContract.LabelsEntry._ID, labelid);
                labelValues.put(DataContract.LabelsEntry.COLUMN_LABELNAME, labelName);
                // And add to table that makes link between events and labels
                eventLabelsValues.put(DataContract.EventsLabelsEntry._ID, eventid);
                eventLabelsValues.put(DataContract.EventsLabelsEntry.COLUMN_LABELID, labelid);

                labelsCVVector.add(labelValues);
                eventLabelsCVVector.add(eventLabelsValues);
            }

            if ( (labelsCVVector.size() > 0) && (eventLabelsCVVector.size() > 0) ) {
                ContentValues[] cvArray;
                // bulk insert labels
                cvArray = new ContentValues[labelsCVVector.size()];
                labelsCVVector.toArray(cvArray);
                this.getContentResolver().bulkInsert(DataContract.LabelsEntry.CONTENT_URI, cvArray);
                // bulk insert event_labels
                cvArray = new ContentValues[eventLabelsCVVector.size()];
                eventLabelsCVVector.toArray(cvArray);
                this.getContentResolver().bulkInsert(DataContract.EventsLabelsEntry.CONTENT_URI, cvArray);

            }
            //**************** LABELS END

        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
    }

    private void handleUpdateEventInfo() {
        Log.v(LOG_TAG, "Started handleUpdateEventInfo");
    }

    private void handleCreateNewEvent() {
        Log.v(LOG_TAG, "Started handleCreateNewEvent");
    }
}
