package com.yentlstorms.virdirapptor.data;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.yentlstorms.virdirapptor.data.DataContract.PersonsEntry;
import com.yentlstorms.virdirapptor.data.DataContract.EventsEntry;
import com.yentlstorms.virdirapptor.data.DataContract.PersonsEventsEntry;
import com.yentlstorms.virdirapptor.data.DataContract.LabelsEntry;
import com.yentlstorms.virdirapptor.data.DataContract.EventsLabelsEntry;
import com.yentlstorms.virdirapptor.data.DataContract.FollowsEntry;

import java.util.ArrayList;

/**
 * Created by Yentl on 4/11/2015.
 */
public class DataDBHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 12;

    static final String DATABASE_NAME = "virdirapptor.db";

    public DataDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // Create a table to hold locations.  A location consists of the string supplied in the
        // location setting, the city name, and the latitude and longitude
        final String SQL_CREATE_PERSONS_TABLE = "CREATE TABLE " + PersonsEntry.TABLE_NAME + " (" +
                PersonsEntry._ID + " INTEGER PRIMARY KEY," +
                PersonsEntry.COLUMN_USERNAME + " TEXT UNIQUE NOT NULL, " +
                PersonsEntry.COLUMN_PERSONNAME + " TEXT NOT NULL, " +
                PersonsEntry.COLUMN_DOB + " TEXT, " +
                PersonsEntry.COLUMN_LOCATION_STREET + " TEXT, " +
                PersonsEntry.COLUMN_LOCATION_NUMBER + " INTEGER, " +
                PersonsEntry.COLUMN_LOCATION_POSTAL + " INTEGER, " +
                PersonsEntry.COLUMN_LOCATION_TOWN + " TEXT, " +
                PersonsEntry.COLUMN_LOCATION_COUNTRY + " TEXT, " +
                PersonsEntry.COLUMN_PICTURE_URL + " TEXT, " +
                PersonsEntry.COLUMN_EMAIL + " TEXT, " +
                PersonsEntry.COLUMN_LAST_CHANGED + " TEXT " +
                " );";

        final String SQL_CREATE_EVENTS_TABLE = "CREATE TABLE " + EventsEntry.TABLE_NAME + " (" +
                EventsEntry._ID + " INTEGER PRIMARY KEY, " +
                EventsEntry.COLUMN_EVENTNAME + " TEXT NOT NULL, " +
                EventsEntry.COLUMN_STARTDATE + " TEXT, " +
                EventsEntry.COLUMN_ENDDATE + " TEXT, " +
                EventsEntry.COLUMN_LOCATION_STREET + " TEXT, " +
                EventsEntry.COLUMN_LOCATION_NUMBER + " INTEGER, " +
                EventsEntry.COLUMN_LOCATION_POSTAL + " INTEGER, " +
                EventsEntry.COLUMN_LOCATION_TOWN + " TEXT, " +
                EventsEntry.COLUMN_LOCATION_COUNTRY + " TEXT, " +
                EventsEntry.COLUMN_PICTURE_URL + " TEXT, " +
                EventsEntry.COLUMN_DESCRIPTION + " TEXT, " +
                EventsEntry.COLUMN_CREATOR_ID + " INTEGER, " +
                EventsEntry.COLUMN_LAST_CHANGED + " TEXT, " +
                " FOREIGN KEY (" + EventsEntry.COLUMN_CREATOR_ID + ") REFERENCES " +
                PersonsEntry.TABLE_NAME + " (" + PersonsEntry._ID + ") " +
                " );";

        final String SQL_CREATE_PERSONS_EVENTS_TABLE = "CREATE TABLE " + PersonsEventsEntry.TABLE_NAME + " (" +
                PersonsEventsEntry._ID + " INTEGER, " +
                PersonsEventsEntry.COLUMN_EVENTID + " INTEGER, " +
                " PRIMARY KEY (" + PersonsEventsEntry._ID + ", " + PersonsEventsEntry.COLUMN_EVENTID + "), " +
                " FOREIGN KEY (" + PersonsEventsEntry._ID + ") REFERENCES " +
                PersonsEntry.TABLE_NAME + " (" + PersonsEntry._ID + "), " +
                " FOREIGN KEY (" + PersonsEventsEntry.COLUMN_EVENTID + ") REFERENCES " +
                EventsEntry.TABLE_NAME + " (" + EventsEntry._ID + ") " +
                " );";

        final String SQL_CREATE_LABELS_TABLE = "CREATE TABLE " + LabelsEntry.TABLE_NAME + " (" +
                LabelsEntry._ID + " INTEGER PRIMARY KEY, " +
                LabelsEntry.COLUMN_LABELNAME + " TEXT UNIQUE NOT NULL " +
                " );";

        final String SQL_CREATE_EVENTS_LABELS_TABLE = "CREATE TABLE " + EventsLabelsEntry.TABLE_NAME + " (" +
                EventsLabelsEntry._ID + " INTEGER, " +
                EventsLabelsEntry.COLUMN_LABELID + " INTEGER, " +
                " PRIMARY KEY (" + EventsLabelsEntry._ID + ", " + EventsLabelsEntry.COLUMN_LABELID + "), " +
                " FOREIGN KEY (" + EventsLabelsEntry._ID + ") REFERENCES " +
                EventsEntry.TABLE_NAME + " (" + EventsEntry._ID + "), " +
                " FOREIGN KEY (" + EventsLabelsEntry.COLUMN_LABELID + ") REFERENCES " +
                LabelsEntry.TABLE_NAME + " (" + LabelsEntry._ID + ") " +
                " );";

        final String SQL_CREATE_FOLLOWS_TABLE = "CREATE TABLE " + FollowsEntry.TABLE_NAME + " (" +
                FollowsEntry._ID + " INTEGER, " +
                FollowsEntry.COLUMN_PERSONTWO_ID + " INTEGER, " +
                " PRIMARY KEY (" + FollowsEntry._ID + ", " + FollowsEntry.COLUMN_PERSONTWO_ID + "), " +
                " FOREIGN KEY (" + FollowsEntry._ID + ") REFERENCES " +
                PersonsEntry.TABLE_NAME + " (" + PersonsEntry._ID + "), " +
                " FOREIGN KEY (" + FollowsEntry.COLUMN_PERSONTWO_ID + ") REFERENCES " +
                PersonsEntry.TABLE_NAME + " (" + PersonsEntry._ID + ") " +
                " );";

        sqLiteDatabase.execSQL(SQL_CREATE_PERSONS_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_EVENTS_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_PERSONS_EVENTS_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_LABELS_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_EVENTS_LABELS_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_FOLLOWS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // For now just completely rebuild the database.
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PersonsEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + EventsEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PersonsEventsEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + LabelsEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + EventsLabelsEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + FollowsEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    /**
     * Following method is used for debugging (option to display database within application)
     */
    public ArrayList<Cursor> getData(String Query){
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[] { "mesage" };
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2= new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);


        try{
            String maxQuery = Query ;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);


            //add value to cursor2
            Cursor2.addRow(new Object[] { "Success" });

            alc.set(1,Cursor2);
            if (null != c && c.getCount() > 0) {


                alc.set(0,c);
                c.moveToFirst();

                return alc ;
            }
            return alc;
        } catch(SQLException sqlEx){
            Log.d("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+sqlEx.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        } catch(Exception ex){

            Log.d("printing exception", ex.getMessage());

            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+ex.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        }


    }
}
