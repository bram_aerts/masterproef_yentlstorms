package com.yentlstorms.virdirapptor.activity;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.yentlstorms.virdirapptor.R;
import com.yentlstorms.virdirapptor.databaseDebug.AndroidDatabaseManager;
import com.yentlstorms.virdirapptor.fragment.HeadHomeFragment;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, HeadHomeFragment.OnFragmentInteractionListener {

    public int userID = 1; // Dummmy user ID, later to be retrieved by logging in.
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    Toolbar mToolbar;

    final String TAG_FRAGMENT_HOME = "TAG_FRAGMENT_HOME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setup DrawerLayout and NavigationView
        mDrawerLayout = (DrawerLayout) findViewById(R.id.main_drawer);
        mNavigationView = (NavigationView) findViewById(R.id.main_navigation_view) ;

        // Inflate the first fragment to start with
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        HeadHomeFragment hf = (mFragmentManager.findFragmentByTag(TAG_FRAGMENT_HOME) == null) ?
                HeadHomeFragment.newInstance() : (HeadHomeFragment) mFragmentManager.findFragmentByTag(TAG_FRAGMENT_HOME);
        mFragmentTransaction.replace(R.id.main_container, hf, TAG_FRAGMENT_HOME).commit();

        // Setup NavigationView Listeners
        mNavigationView.setNavigationItemSelectedListener(this);

        // Setup toolbar so it allows to toggle the navigation drawer
        mToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        ActionBarDrawerToggle mDrawerToggle =
                new ActionBarDrawerToggle(this,mDrawerLayout, mToolbar, R.string.app_name, R.string.app_name);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
    }

    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.main_drawer);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        // TODO: change to switch statement
        if (id == R.id.nav_item_home) {
            mToolbar.setTitle(getString(R.string.option_title_home));

            FragmentManager fragmentManager = getSupportFragmentManager();
            HeadHomeFragment hf = (fragmentManager.findFragmentByTag(TAG_FRAGMENT_HOME) == null) ?
                    HeadHomeFragment.newInstance() : (HeadHomeFragment) fragmentManager.findFragmentByTag(TAG_FRAGMENT_HOME);
            fragmentManager.beginTransaction()
                    .replace(R.id.main_container, hf, TAG_FRAGMENT_HOME)
                    .commit();

        } else if (id == R.id.nav_item_event) {
            mToolbar.setTitle(getString(R.string.option_title_event));
        } else if (id == R.id.nav_item_person) {
            mToolbar.setTitle(getString(R.string.option_title_person));
        } else if (id == R.id.nav_item_database) {
            Intent dbmanager = new Intent(this, AndroidDatabaseManager.class);
            startActivity(dbmanager);
        } else if (id == R.id.nav_item_share) {

        } else if (id == R.id.nav_item_send) {

        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.v("MainActivity", "onActivityResult in MainActivity");
        super.onActivityResult(requestCode, resultCode, data);
/*
        // TODO: try and fix this so onActivityResult does get passed on automatically
        FragmentManager fragmentManager = getSupportFragmentManager();
        HeadHomeFragment hf = (fragmentManager.findFragmentByTag(TAG_FRAGMENT_HOME) == null) ?
                HeadHomeFragment.newInstance() : (HeadHomeFragment) fragmentManager.findFragmentByTag(TAG_FRAGMENT_HOME);
        hf.onActivityResult(requestCode, resultCode, data);
        */
    }
}
