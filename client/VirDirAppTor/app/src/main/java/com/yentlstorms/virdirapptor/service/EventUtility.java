package com.yentlstorms.virdirapptor.service;

import android.util.Log;

import com.yentlstorms.virdirapptor.data.DataContract;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Yentl on 2/03/2016.
 */
public class EventUtility {

    public static final String LOG_TAG = EventUtility.class.getSimpleName();

    /**
     *
     * @param startDate String representing the start time of the event in dd/MM/yyyy hh:mm:ss format
     * @param endDate String representing the end time of the event in dd/MM/yyyy hh:mm:ss format
     * @return String representing a user-friendly representation of the 2 input dates, in MMM dd, HH:mm format
     */
    public static String datesToEventListString(String startDate, String endDate) {
        return EventUtility.dateToEventListString(startDate) + " - " + EventUtility.dateToEventListString(endDate);
    }

    public static String dateToEventListString(String date) {
        try {
            // Make a Date out of the String
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date d = sdf.parse(date);
            Calendar c = Calendar.getInstance();
            c.setTime(d);

            // Make a new String out of this Date
            return new SimpleDateFormat("MMM dd yyyy, HH:mm").format(d);
        } catch (ParseException pe) {
            Log.e(LOG_TAG, pe.toString());
            return "";
        }
    }

}
