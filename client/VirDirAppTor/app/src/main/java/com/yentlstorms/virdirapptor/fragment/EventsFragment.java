package com.yentlstorms.virdirapptor.fragment;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.support.v4.app.Fragment;
import android.widget.ListView;

import com.yentlstorms.virdirapptor.R;
import com.yentlstorms.virdirapptor.adapter.EventAdapter;
import com.yentlstorms.virdirapptor.adapter.PersonAdapter;
import com.yentlstorms.virdirapptor.data.DataContract;

public class EventsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private EventAdapter mEventAdapter;
    private ListView mListView;

    private static final int EVENTSFRAGMENT_LOADER = 5438;

    private static final String[] EVENTSFRAGMENT_COLUMNS = {
            DataContract.EventsEntry.TABLE_NAME + "." + DataContract.EventsEntry._ID,
            DataContract.EventsEntry.COLUMN_EVENTNAME,
            DataContract.EventsEntry.COLUMN_STARTDATE,
            DataContract.EventsEntry.COLUMN_ENDDATE,
            DataContract.EventsEntry.COLUMN_PICTURE_URL
    };

    public static final int COL_EVENTS_ID = 0;
    public static final int COL_EVENTS_EVENTNAME = 1;
    public static final int COL_EVENTS_STARTDATE = 2;
    public static final int COL_EVENTS_ENDDATE = 3;
    public static final int COL_EVENTS_PICTURE_URL = 4;

    public static EventsFragment newInstance() {
        EventsFragment fragment = new EventsFragment();
        return fragment;
    }

    public EventsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mEventAdapter = new EventAdapter(getActivity(), null, 0);

        View rootView = inflater.inflate(R.layout.fragment_events, container, false);

        // Get a reference to the ListView, and attach this adapter to it.
        mListView = (ListView) rootView.findViewById(R.id.fragment_events_listview);
        mListView.setAdapter(mEventAdapter);
        // TODO: add setOnItemClickListener to the listview

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getLoaderManager().initLoader(EVENTSFRAGMENT_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {

        //String sortOrder = WeatherContract.WeatherEntry.COLUMN_DATE + " ASC";

        // TODO: Currently use 1 as given id, later on change to user's id
        Uri personsEventsWithIDUri = DataContract.PersonsEventsEntry.buildPersonsEventsUri(1);

        return new CursorLoader(getActivity(),
                personsEventsWithIDUri,
                EVENTSFRAGMENT_COLUMNS,    // projection[]
                null,                       // selection        done in the provider itself.
                null,                       // selectionArgs[]
                null                        // sort order
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mEventAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mEventAdapter.swapCursor(null);
    }

}
