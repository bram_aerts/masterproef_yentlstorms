package com.yentlstorms.virdirapptor.excludeForNow;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by Yentl on 23/11/2015.
 */
public class ExtendedDatePicker extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    public class MyDatePickerDialog extends DatePickerDialog {
        private Calendar calendar;
        private final String format = "EEEE, MMMM dd, yyyy";

        // Regular constructor
        public MyDatePickerDialog(Context context, OnDateSetListener callBack, int year, int monthOfYear, int dayOfMonth) {
            super(context, callBack, year, monthOfYear, dayOfMonth);
            calendar = Calendar.getInstance();
        }

        // Short constructor
        public MyDatePickerDialog(Context context, OnDateSetListener callBack, Calendar calendar) {
            super(context, callBack, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            this.calendar = calendar;
        }

        @Override
        public void onDateChanged(DatePicker view, int year, int month, int day) {
            super.onDateChanged(view, year, month, day);
            //Log.v("DPF", year+" -- "+month+" -- "+day);
            Intent intent = new Intent();
            intent.putExtra("year", year);
            intent.putExtra("month", month);
            intent.putExtra("day", day);
            //getTargetFragment().onActivityResult(getTargetRequestCode(), 0, intent);
        }
    }


    public static ExtendedDatePicker newInstance() {
        ExtendedDatePicker frag = new ExtendedDatePicker();
        return frag;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle dateBundle = getArguments().getBundle("currentValue");
        DatePickerDialog dpDialog = new MyDatePickerDialog(
                getActivity(),
                this,
                dateBundle.getInt("year"),
                dateBundle.getInt("month"),
                dateBundle.getInt("day")
        );
        /*DatePickerDialog dpDialog = new DatePickerDialog(
                getActivity(),
                this,
                dateBundle.getInt("year"),
                dateBundle.getInt("month"),
                dateBundle.getInt("day")
        );*/
        dpDialog.setTitle(getArguments().getString("title"));


        return dpDialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        //TODO: clean this up
        Intent intent = new Intent();
        intent.putExtra("year", year);
        intent.putExtra("month", monthOfYear);
        intent.putExtra("day", dayOfMonth);
        getTargetFragment().onActivityResult(getTargetRequestCode(), 1, intent);
    }
}