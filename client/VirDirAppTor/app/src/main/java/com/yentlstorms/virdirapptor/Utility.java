package com.yentlstorms.virdirapptor;


import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Yentl on 11/11/2015.
 */
public class Utility {

    public static final String LOG_TAG = Utility.class.getSimpleName();

    /**
     * Helper method to check equality on string, INCLUDING NULL.
     * @param a First string to compare
     * @param b Second string to compare
     * @return true if a and b are equal, false if not
     */
    public static boolean equals(String a, String b) {
        if ( (a == null) || (b == null) ) {
            if ( (a == null) && (b == null) ) {
                return true;
            }
        } else if ( a.equals(b) ) {
            return true;
        }
        return false;
    }


    /**
     * @param dateBundle A bundle containing the day, month and year as an integer (as set in personfragment)
     * @return A formatted string in MMM dd, yyyy format
     */
    public static String dateBundleToFriendlyDate(Bundle dateBundle) {
        Date date = new GregorianCalendar(
                dateBundle.getInt(Constants.BUNDLE_KEY_DATE_YEAR),
                dateBundle.getInt(Constants.BUNDLE_KEY_DATE_MONTH),
                dateBundle.getInt(Constants.BUNDLE_KEY_DATE_DAY))
                .getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
        return sdf.format(date);
    }

    /**
     * @param dateBundle a bundle containing the day, month and year as integers (as set in personfragment)
     * @return A formatted string in dd/mm/yyyy format (as required by database)
     */
    public static String dateBundleToDatabaseDate(Bundle dateBundle) {
        Date date = new GregorianCalendar(
                dateBundle.getInt(Constants.BUNDLE_KEY_DATE_YEAR),
                dateBundle.getInt(Constants.BUNDLE_KEY_DATE_MONTH),
                dateBundle.getInt(Constants.BUNDLE_KEY_DATE_DAY)).getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(date);
    }

    /**
     * @param ddmmyyyyString String representing a date in dd/mm/yyyy format
     * @return Calendar object representing the time indicated by ddmmyyyyString
     */
    public static Calendar dateStringToCalendar(String ddmmyyyyString) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date d = sdf.parse(ddmmyyyyString);
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            return c;
        } catch (ParseException pe) {
            // TODO: change to default values, for now take current time.
            Calendar c = Calendar.getInstance();
            return c;
        }
    }

    /**
     * Used to download an image from server to the local system
     * @param source URL indicating the image's location
     * @param destination File indicating where to save the image on the local system
     * @return true if download successful, false if error occured.
     */
    public static boolean downloadImageFromServer(URL source, File destination) {
        if (!destination.exists()) {
            try {
                destination.createNewFile();
            } catch (IOException ioe) {
                Log.v(LOG_TAG, "downloadImageFromServer: could not createNewFile()");
                return false;
            }
        } else {
            // All images have a unique name (using ID and timestamp), so if image with name already exists
            // We don't have to download image we already have on system
            Log.v(LOG_TAG, "The given destination (" + destination.getAbsolutePath() + ") already exists, no need to redownload.");
            return true;
        }
        if (!destination.isFile()) {
            Log.v(LOG_TAG, "downloadImageFromServer: destination " + destination.getAbsolutePath() + " is not a file!");
            return false;
        }

        try {
            URLConnection urlConnection = source.openConnection();
            if ( !(urlConnection instanceof HttpURLConnection) ) {
                return false;
            }
            HttpURLConnection httpConnection = (HttpURLConnection) urlConnection;
            if (httpConnection.getResponseCode() == 404) {
                Log.v(LOG_TAG, "Could not open connection to "+source.getPath()+": 404 code.");
                return false;
            }
            InputStream input = httpConnection.getInputStream();
            try {
                OutputStream output = new FileOutputStream(destination);
                try {
                    byte[] buffer = new byte[1024];
                    int bytesRead = 0;
                    while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
                        output.write(buffer, 0, bytesRead);
                    }
                } finally {
                    output.close();
                }
            } finally {
                input.close();
            }
            return true;
        } catch (IOException ioe) {
            Log.v(LOG_TAG, "downloadImageFromServer: IOException: " + ioe.toString());
            return false;
        }
    }

    /**
     *
     * @param context
     * @return File representing the directory people's profile pictures are saved.
     */
    public static File getProfilePicturesDirectory(Context context) {
        String profilePicturesDirPath =
                context.getFilesDir().getAbsolutePath() + File.separator + Constants.DIR_PROFILE_PICTURES;
        File profilePicturesDir = new File(profilePicturesDirPath);
        if ( !profilePicturesDir.exists() ) {
            profilePicturesDir.mkdirs();
        }
        return profilePicturesDir;
    }

    public static File getEventPicturesDirectory(Context context) {
        String eventPicturesDirPath =
                context.getFilesDir().getAbsolutePath() + File.separator + Constants.DIR_EVENT_PICTURES;
        File eventPicturesDir = new File(eventPicturesDirPath);
        if ( !eventPicturesDir.exists() ) {
            eventPicturesDir.mkdirs();
        }
        return eventPicturesDir;
    }

    /**
     * Method used for creating unique Strings, based on time of function call.
     * @return String representing the time in yyyyMMddHHmmSS format.
     */
    public static String createCurrentTime() {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    }

    public static String getJSONStringFromServer(Uri uriToGet) {
        Log.v(LOG_TAG, "Started getJSONStringFromServer");

        // These two need to be declared outside the try/catch
        // so that they can be closed in the finally block.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        // Will contain the raw JSON response as a string.
        String responseJSONString;
        try {
            URL url = new URL(uriToGet.toString());

            // Create the request to OpenWeatherMap, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return null;
            }
            responseJSONString = buffer.toString();
            Log.v(LOG_TAG, "JSON string:" + responseJSONString);
            return responseJSONString;
        } catch (ConnectException e) {
            Log.e(LOG_TAG, "Error ", e);
            return null;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error ", e);
            return null;
            // If the code didn't successfully get the weather data, there's no point in attempting
            // to parse it.
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(LOG_TAG, "Error closing stream", e);
                }
            }
        }
    }

    /*
    public static boolean uploadImageToServer(File imageFile) {
        if (!imageFile.exists()) {
            Log.v(LOG_TAG, "uploadImageToServer: " + imageFile.getAbsolutePath() + " does not exist");
            return false;
        }
        if (!imageFile.isFile()) {
            Log.v(LOG_TAG, "uploadImageToServer: " + imageFile.getAbsolutePath() + "is not a file.");
            return false;
        }

        Log.v(LOG_TAG, "uploadImageToServer: no errors on input file.");
        DataOutputStream outputStream = null;
        DataInputStream inputStream = null;
        HttpURLConnection connection = null;
        try {
            FileInputStream fileInputStream = new FileInputStream(imageFile);

            final String SERVER_UPLOAD_URL = Constants.SERVER_BASE_URL + File.separator + "upload";
            URL uploadURL = new URL(SERVER_UPLOAD_URL);
            connection = (HttpURLConnection) uploadURL.openConnection();

            // Allow input/output
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);

            // Set appropriate method/header fields
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + Constants.boundary);

            outputStream = new DataOutputStream( connection.getOutputStream() );
            outputStream.writeBytes(Constants.twoHyphens + Constants.boundary + Constants.lineEnd);
            outputStream.writeBytes("Content-Disposition: form-data; name=\"file\";filename=\"" +
                    imageFile.getName() + "\"" + Constants.lineEnd);
            outputStream.writeBytes(Constants.lineEnd);

            int maxBufferSize = 1*1024*1024;
            int bytesAvailable = fileInputStream.available();
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);
            byte[] buffer = new byte[bufferSize];

            // Read file
            int bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0)
            {
                outputStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            outputStream.writeBytes(Constants.lineEnd);
            outputStream.writeBytes(Constants.twoHyphens + Constants.boundary + Constants.twoHyphens + Constants.lineEnd);

            // Responses from the server (code and message)
            int serverResponseCode = connection.getResponseCode();
            String serverResponseMessage = connection.getResponseMessage();
            Log.v(LOG_TAG, "Response: "+serverResponseCode + " -- " + serverResponseMessage);
            fileInputStream.close();
            outputStream.flush();
            outputStream.close();

            connection.disconnect();
        } catch (Exception e) {
            Log.v(LOG_TAG, "Exception: "+e);
        }

        return true;
    }*/
}
