package com.yentlstorms.virdirapptor.fragment;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.yentlstorms.virdirapptor.Constants;
import com.yentlstorms.virdirapptor.R;
import com.yentlstorms.virdirapptor.Utility;
import com.yentlstorms.virdirapptor.data.DataContract;
import com.yentlstorms.virdirapptor.dialog.ChangeLocationFragment;
import com.yentlstorms.virdirapptor.dialog.DatePickerFragment;
import com.yentlstorms.virdirapptor.dialog.ProfilePictureFragment;
import com.yentlstorms.virdirapptor.dialog.SingleInputFragment;
import com.yentlstorms.virdirapptor.service.UserInfoService;

import java.io.File;
import java.util.Calendar;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserFragment extends Fragment
        implements LoaderManager.LoaderCallbacks<Cursor>, View.OnClickListener {

    private final String LOG_TAG = UserFragment.class.getSimpleName();

    boolean mDataChanged = false;
    Bundle mDataValues = new Bundle();

    //private OnFragmentInteractionListener mListener;
    ViewHolder mViewHolder;

    private static final int USERFRAGMENT_LOADER = 0;

    public static final String[] PERSONFRAGMENT_COLUMNS = {
            DataContract.PersonsEntry._ID,
            DataContract.PersonsEntry.COLUMN_USERNAME,
            DataContract.PersonsEntry.COLUMN_PERSONNAME,
            DataContract.PersonsEntry.COLUMN_DOB,
            DataContract.PersonsEntry.COLUMN_LOCATION_STREET,
            DataContract.PersonsEntry.COLUMN_LOCATION_NUMBER,
            DataContract.PersonsEntry.COLUMN_LOCATION_POSTAL,
            DataContract.PersonsEntry.COLUMN_LOCATION_TOWN,
            DataContract.PersonsEntry.COLUMN_LOCATION_COUNTRY,
            DataContract.PersonsEntry.COLUMN_PICTURE_URL,
            DataContract.PersonsEntry.COLUMN_EMAIL,
            DataContract.PersonsEntry.COLUMN_LAST_CHANGED
    };

    public static final int COL_PERSONS_ID = 0;
    public static final int COL_PERSONS_USERNAME = 1;
    public static final int COL_PERSONS_PERSONNAME = 2;
    public static final int COL_PERSONS_DOB = 3;
    public static final int COL_PERSONS_LOCATION_STREET = 4;
    public static final int COL_PERSONS_LOCATION_NUMBER = 5;
    public static final int COL_PERSONS_LOCATION_POSTAL = 6;
    public static final int COL_PERSONS_LOCATION_TOWN = 7;
    public static final int COL_PERSONS_LOCATION_COUNTRY = 8;
    public static final int COL_PERSONS_PICTURE_URL = 9;
    public static final int COL_PERSONS_EMAIL = 10;
    public static final int COL_PERSONS_LASTCHANGED = 11;

    final String TAG_DIALOG_PERSONFRAGMENT_PERSONNAME = "TAG_DIALOG_PERSONFRAGMENT_PERSONNAME";
    final String TAG_DIALOG_PERSOFRAGMENT_DOB = "TAG_DIALOG_PERSOFRAGMENT_DOB";
    final String TAG_DIALOG_PERSONFRAGMENT_EMAIL = "TAG_DIALOG_PERSONFRAGMENT_EMAIL";
    final String TAG_DIALOG_PERSONFRAGMENT_LOCATION = "TAG_DIALOG_PERSONFRAGMENT_LOCATION";
    final String TAG_DIALOG_PERSONFRAGMENT_PICTURE = "TAG_DIALOG_PERSONFRAGMENT_PICTURE";

    static final int REQUEST_IMAGE_CAPTURE = 9864;

    private File mUncroppedPictureFile;
    private File mCroppedPictureFile;
    //private Uri mUncroppedPictureUri;
    //private Uri mCroppedPictureUri;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment UserFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserFragment newInstance() {
        UserFragment fragment = new UserFragment();
        return fragment;
    }

    public UserFragment() {
        // Required empty public constructor
    }

    @Override
    public void onClick(View v) {
        Bundle args;
        switch (v.getId()) {
            case R.id.fragment_person_name :
                DialogFragment siPersonNameFragment = SingleInputFragment.newInstance();
                siPersonNameFragment.setTargetFragment(this, COL_PERSONS_PERSONNAME);
                args = new Bundle();
                args.putString("title", getResources().getString(R.string.title_dialog_personname));
                args.putCharSequence("currentValue",
                        mDataValues.getString(Constants.BUNDLE_KEY_PERSONNAME));
                siPersonNameFragment.setArguments(args);
                siPersonNameFragment.show(getFragmentManager(), TAG_DIALOG_PERSONFRAGMENT_PERSONNAME);
                break;
            case R.id.fragment_person_dob :
                DialogFragment dpFragment = DatePickerFragment.newInstance();
                // TODO: change to current value of database + setTargetFragment requestCode clean up
                dpFragment.setTargetFragment(this, COL_PERSONS_DOB);
                args = new Bundle();
                args.putString("title", getResources().getString(R.string.title_dialog_dob));
                args.putBundle("currentValue",
                        mDataValues.getBundle(Constants.BUNDLE_KEY_DOB));
                dpFragment.setArguments(args);
                dpFragment.show(getFragmentManager(), TAG_DIALOG_PERSOFRAGMENT_DOB);
                break;
            case R.id.fragment_person_email :
                DialogFragment siEmailFragment = SingleInputFragment.newInstance();
                siEmailFragment.setTargetFragment(this, COL_PERSONS_EMAIL);
                args = new Bundle();
                args.putString("title", getResources().getString(R.string.title_dialog_email));
                args.putCharSequence("currentValue",
                        mDataValues.getString(Constants.BUNDLE_KEY_EMAIL));
                siEmailFragment.setArguments(args);
                siEmailFragment.show(getFragmentManager(), TAG_DIALOG_PERSONFRAGMENT_EMAIL);
                break;
            case R.id.fragment_person_locationstreet_number :
                // NO BREAK, locationstreet and ziptowncountry opens the same dialog!
            case R.id.fragment_person_locationzip_town_country :
                DialogFragment locationFragment = ChangeLocationFragment.newInstance();
                locationFragment.setTargetFragment(this, COL_PERSONS_LOCATION_STREET);
                args = new Bundle();
                args.putString("title", getResources().getString(R.string.title_dialog_location));
                args.putBundle("currentValue",
                        mDataValues.getBundle(Constants.BUNDLE_KEY_LOCATION));
                locationFragment.setArguments(args);
                locationFragment.show(getFragmentManager(), TAG_DIALOG_PERSONFRAGMENT_LOCATION);
                break;
            case R.id.fragment_person_save :
                saveChanges();
                break;
            case R.id.fragment_person_discard :
                discardChanges();
                break;
            case R.id.fragment_person_icon :
                DialogFragment profPictureFragment = ProfilePictureFragment.newInstance();
                profPictureFragment.setTargetFragment(this, COL_PERSONS_PICTURE_URL);
                profPictureFragment.show(getFragmentManager(), TAG_DIALOG_PERSONFRAGMENT_PICTURE);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.v(LOG_TAG, "ActivityResult: " + requestCode + " -- " + resultCode);

        switch (requestCode) {
            case COL_PERSONS_PICTURE_URL :
                String imageFilename = data.getStringExtra(Constants.BUNDLE_KEY_CROPPED_PICTURE);
                mCroppedPictureFile = new File(Utility.getProfilePicturesDirectory(getContext()),
                        imageFilename);
                Log.v(LOG_TAG, mCroppedPictureFile.getPath());
                this.mDataValues.putString(Constants.BUNDLE_KEY_CROPPED_PICTURE, imageFilename);
                this.mDataChanged = true;
                mViewHolder.iconView.setImageDrawable(null);
                mViewHolder.iconView.setImageURI(Uri.fromFile(mCroppedPictureFile));
                break;
            case COL_PERSONS_PERSONNAME :
                if ( !Utility.equals(this.mDataValues.getString(Constants.BUNDLE_KEY_PERSONNAME), data.getStringExtra("returnValue")) ) {
                    this.mDataChanged = true;
                }
                this.mDataValues.putString(Constants.BUNDLE_KEY_PERSONNAME, data.getStringExtra("returnValue"));
                mViewHolder.personnameView.setText(this.mDataValues.getString(Constants.BUNDLE_KEY_PERSONNAME));
                break;
            case COL_PERSONS_DOB :
                // TODO: change to check for mDataChanged
                this.mDataValues.putBundle(Constants.BUNDLE_KEY_DOB, data.getExtras());
                this.mDataChanged = true;
                mViewHolder.dobView.setText(Utility.dateBundleToFriendlyDate(this.mDataValues.getBundle(Constants.BUNDLE_KEY_DOB)));
                break;
            case COL_PERSONS_EMAIL :
                if ( !Utility.equals(this.mDataValues.getString(Constants.BUNDLE_KEY_EMAIL), data.getStringExtra("returnValue")) ) {
                    this.mDataChanged = true;
                    this.mDataValues.putString(Constants.BUNDLE_KEY_EMAIL, data.getStringExtra("returnValue"));
                }
                mViewHolder.emailView.setText(this.mDataValues.getString(Constants.BUNDLE_KEY_EMAIL));
                break;
            case COL_PERSONS_LOCATION_STREET :
                //TODO: change to check for mDataChanged + how to update gui (now updating all views)
                mDataChanged = true;
                this.mDataValues.putBundle(Constants.BUNDLE_KEY_LOCATION, data.getExtras());
                updateGUI();
                break;
            default:
                List<Fragment> fragments = getChildFragmentManager().getFragments();
                if (fragments != null) {
                    for (Fragment fragment : fragments) {
                        if (fragment != null) {
                            fragment.onActivityResult(requestCode, resultCode, data);
                        }
                    }
                }
                // super.onActivityResult(requestCode, resultCode, data);
                break;
        }
        if (this.mDataChanged) {
            showSaveDiscardButtons(true);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_person, container, false);
        mViewHolder = new ViewHolder(rootView.findViewById(R.id.fragment_person_content));

        // add the onClickListeners for the buttons
        mViewHolder.saveView.setOnClickListener(this);
        mViewHolder.discardView.setOnClickListener(this);
        mViewHolder.iconView.setOnClickListener(this);
        mViewHolder.usernameView.setOnClickListener(this);
        mViewHolder.personnameView.setOnClickListener(this);
        mViewHolder.dobView.setOnClickListener(this);
        mViewHolder.emailView.setOnClickListener(this);
        mViewHolder.streetNumberView.setOnClickListener(this);
        mViewHolder.zipTownCountryView.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if ( (savedInstanceState != null) && (savedInstanceState.getBundle("mDataValues") != null) ) {
            this.mDataChanged = true;
            this.mDataValues = savedInstanceState.getBundle("mDataValues");
            if (mDataValues.getString(Constants.BUNDLE_KEY_UNCROPPED_PICTURE) == null) {
                mUncroppedPictureFile = null;
            } else {
                mUncroppedPictureFile = new File(Utility.getProfilePicturesDirectory(getContext()),
                        mDataValues.getString(Constants.BUNDLE_KEY_UNCROPPED_PICTURE));
            }
            if (mDataValues.getString(Constants.BUNDLE_KEY_CROPPED_PICTURE) == null) {
                mCroppedPictureFile = null;
            } else {
                mCroppedPictureFile = new File(Utility.getProfilePicturesDirectory(getContext()),
                        mDataValues.getString(Constants.BUNDLE_KEY_CROPPED_PICTURE));
            }
            this.updateGUI();
        } else {
            getLoaderManager().initLoader(USERFRAGMENT_LOADER, null, this);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (mDataChanged) {
            if (mUncroppedPictureFile != null && mUncroppedPictureFile.exists()) {
                mDataValues.putString(Constants.BUNDLE_KEY_UNCROPPED_PICTURE, mUncroppedPictureFile.getName());
            } else {
                mDataValues.putString(Constants.BUNDLE_KEY_UNCROPPED_PICTURE, null);
            }
            if (mCroppedPictureFile != null && mCroppedPictureFile.exists()) {
                mDataValues.putString(Constants.BUNDLE_KEY_CROPPED_PICTURE, mCroppedPictureFile.getName());
            } else {
                mDataValues.putString(Constants.BUNDLE_KEY_CROPPED_PICTURE, null);
            }

            outState.putBundle("mDataValues", mDataValues);
        }
    }



    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        // This is called when a new Loader needs to be created.  This
        // fragment only uses one loader, so we don't care about checking the id.

        // TODO: Currently use 1 as given id, later on change to user's id
        Uri personsWithIDUri = DataContract.PersonsEntry.buildPersonsUri(1);

        return new CursorLoader(getActivity(),
                personsWithIDUri,
                PERSONFRAGMENT_COLUMNS,
                null,
                null,
                null);
    }

    public static class ViewHolder {
        final LinearLayout saveDiscardLayout;
        final Button saveView;
        final Button discardView;

        final ImageView iconView;
        final Button usernameView;
        final Button personnameView;
        final Button dobView;
        final Button emailView;
        final Button streetNumberView;
        final Button zipTownCountryView;

        public ViewHolder(View view) {
            saveDiscardLayout = (LinearLayout) view.findViewById(R.id.fragment_person_save_discard_layout);
            saveView = (Button) view.findViewById(R.id.fragment_person_save);
            discardView = (Button) view.findViewById(R.id.fragment_person_discard);

            iconView = (ImageView) view.findViewById(R.id.fragment_person_icon);
            usernameView = (Button) view.findViewById(R.id.person_bottom_sheet_username);
            personnameView = (Button) view.findViewById(R.id.fragment_person_name);
            dobView = (Button) view.findViewById(R.id.fragment_person_dob);
            emailView = (Button) view.findViewById(R.id.fragment_person_email);
            streetNumberView = (Button) view.findViewById(R.id.fragment_person_locationstreet_number);
            zipTownCountryView = (Button) view.findViewById(R.id.fragment_person_locationzip_town_country);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (mDataChanged) {
            // make sure to not overload data that has been changed.
            return;
        }
        if (data.getCount() < 1) {
            Log.v(LOG_TAG, "data.getCount() < 1");
            CharSequence text = "Could not find your userdata in database";

            Toast toast = Toast.makeText(getContext(), text, Toast.LENGTH_SHORT);
            toast.show();
            return;
        }
        data.moveToFirst();

        this.mDataValues.putString(Constants.BUNDLE_KEY_CROPPED_PICTURE, data.getString(UserFragment.COL_PERSONS_PICTURE_URL));
        mCroppedPictureFile = new File(Utility.getProfilePicturesDirectory(getContext()),
                data.getString(UserFragment.COL_PERSONS_PICTURE_URL));

        // Read data from cursor and add it to the mDataValues bundle
        this.mDataValues.putString(Constants.BUNDLE_KEY_USERNAME, data.getString(UserFragment.COL_PERSONS_USERNAME));
        this.mDataValues.putString(Constants.BUNDLE_KEY_PERSONNAME, data.getString(UserFragment.COL_PERSONS_PERSONNAME));

        Bundle dobBundle = new Bundle();
        Calendar c = Utility.dateStringToCalendar(data.getString(UserFragment.COL_PERSONS_DOB));
        dobBundle.putInt(Constants.BUNDLE_KEY_DATE_DAY, c.get(Calendar.DAY_OF_MONTH));
        dobBundle.putInt(Constants.BUNDLE_KEY_DATE_MONTH, c.get(Calendar.MONTH));
        dobBundle.putInt(Constants.BUNDLE_KEY_DATE_YEAR, c.get(Calendar.YEAR));
        this.mDataValues.putBundle(Constants.BUNDLE_KEY_DOB, dobBundle);

        this.mDataValues.putString(Constants.BUNDLE_KEY_EMAIL, data.getString(UserFragment.COL_PERSONS_EMAIL));

        Bundle locationBundle = new Bundle();
        locationBundle.putString(Constants.BUNDLE_KEY_LOCATION_STREET, data.getString(UserFragment.COL_PERSONS_LOCATION_STREET));
        locationBundle.putInt(Constants.BUNDLE_KEY_LOCATION_NUMBER, data.getInt(UserFragment.COL_PERSONS_LOCATION_NUMBER));
        locationBundle.putInt(Constants.BUNDLE_KEY_LOCATION_ZIP, data.getInt(UserFragment.COL_PERSONS_LOCATION_POSTAL));
        locationBundle.putString(Constants.BUNDLE_KEY_LOCATION_TOWN, data.getString(UserFragment.COL_PERSONS_LOCATION_TOWN));
        locationBundle.putString(Constants.BUNDLE_KEY_LOCATION_COUNTRY, data.getString(UserFragment.COL_PERSONS_LOCATION_COUNTRY));
        this.mDataValues.putBundle(Constants.BUNDLE_KEY_LOCATION, locationBundle);

        updateGUI();
    }

    public void updateGUI() {
        if (this.mDataValues == null) {
            // Should not be possible on the times we call this function
            return;
        }
        if (this.mDataChanged) {
            showSaveDiscardButtons(true);
        }
        // TODO: change back to picture display
        if (mCroppedPictureFile.exists()) {
            mViewHolder.iconView.setImageURI(Uri.fromFile(mCroppedPictureFile));
        } else {
            mViewHolder.iconView.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.user));
        }
        //mViewHolder.iconView.setImageResource(mDataValues.getInt(Constants.BUNDLE_KEY_IMAGE));
        mViewHolder.usernameView.setText(mDataValues.getString(Constants.BUNDLE_KEY_USERNAME));
        mViewHolder.personnameView.setText(mDataValues.getString(Constants.BUNDLE_KEY_PERSONNAME));
        mViewHolder.dobView.setText(Utility.dateBundleToFriendlyDate(mDataValues.getBundle(Constants.BUNDLE_KEY_DOB)));
        mViewHolder.emailView.setText(mDataValues.getString(Constants.BUNDLE_KEY_EMAIL));
        Bundle locationBundle = mDataValues.getBundle(Constants.BUNDLE_KEY_LOCATION);
        if ( locationBundle == null ) { locationBundle = new Bundle(); }
        // TODO: change concatenated string to placeholders style
        mViewHolder.streetNumberView.setText(
                locationBundle.getString(Constants.BUNDLE_KEY_LOCATION_STREET) + " " + locationBundle.getInt(Constants.BUNDLE_KEY_LOCATION_NUMBER));
        mViewHolder.zipTownCountryView.setText(
                locationBundle.getInt(Constants.BUNDLE_KEY_LOCATION_ZIP) + ", " +
                        locationBundle.getString(Constants.BUNDLE_KEY_LOCATION_TOWN) + ", " +
                        locationBundle.getString(Constants.BUNDLE_KEY_LOCATION_COUNTRY));
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // TODO: change to an actual action if needed
    }
/*
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
*/
    public void showSaveDiscardButtons(boolean bool) {
        if (bool) {
            mViewHolder.saveDiscardLayout.setVisibility(View.VISIBLE);
            return;
        }
        mViewHolder.saveDiscardLayout.setVisibility(View.GONE);
    }

    public void saveChanges() {
        ContentValues personValues = new ContentValues();

        String lastChanged = Utility.createCurrentTime();

        //personValues.put(DataContract.PersonsEntry._ID, 1);
        personValues.put(DataContract.PersonsEntry.COLUMN_USERNAME, mDataValues.getString(Constants.BUNDLE_KEY_USERNAME));
        personValues.put(DataContract.PersonsEntry.COLUMN_PERSONNAME, mDataValues.getString(Constants.BUNDLE_KEY_PERSONNAME));
        personValues.put(DataContract.PersonsEntry.COLUMN_DOB,
                Utility.dateBundleToDatabaseDate(mDataValues.getBundle(Constants.BUNDLE_KEY_DOB)));
        Bundle locationBundle = mDataValues.getBundle(Constants.BUNDLE_KEY_LOCATION);
        personValues.put(DataContract.PersonsEntry.COLUMN_LOCATION_STREET, locationBundle.getString(Constants.BUNDLE_KEY_LOCATION_STREET));
        personValues.put(DataContract.PersonsEntry.COLUMN_LOCATION_NUMBER, locationBundle.getInt(Constants.BUNDLE_KEY_LOCATION_NUMBER));
        personValues.put(DataContract.PersonsEntry.COLUMN_LOCATION_POSTAL, locationBundle.getInt(Constants.BUNDLE_KEY_LOCATION_ZIP));
        personValues.put(DataContract.PersonsEntry.COLUMN_LOCATION_TOWN, locationBundle.getString(Constants.BUNDLE_KEY_LOCATION_TOWN));
        personValues.put(DataContract.PersonsEntry.COLUMN_LOCATION_COUNTRY, locationBundle.getString(Constants.BUNDLE_KEY_LOCATION_COUNTRY));
        personValues.put(DataContract.PersonsEntry.COLUMN_PICTURE_URL,
                mCroppedPictureFile.getName());
        personValues.put(DataContract.PersonsEntry.COLUMN_EMAIL, mDataValues.getString(Constants.BUNDLE_KEY_EMAIL));
        personValues.put(DataContract.PersonsEntry.COLUMN_LAST_CHANGED, lastChanged);

        // TODO: change the ID to the actual logged in user ID intead of default 1
        getContext().getContentResolver().update(
                DataContract.PersonsEntry.CONTENT_URI,
                personValues,
                DataContract.PersonsEntry._ID + " = ? ",
                new String[]{String.valueOf(1)}
        );

        // TODO: change to actual personid
        mDataValues.putInt(Constants.BUNDLE_KEY_PERSONID, 1);
        mDataValues.putString(Constants.BUNDLE_KEY_LASTCHANGED, lastChanged);
        UserInfoService.startActionUpdateUserInfo(getContext(), mDataValues);

        mDataChanged = false;
        this.showSaveDiscardButtons(false);
    }

    public void discardChanges() {
        mDataChanged = false;
        this.showSaveDiscardButtons(false);
        getLoaderManager().restartLoader(USERFRAGMENT_LOADER, null, this);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
