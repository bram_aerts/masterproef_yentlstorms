
package com.yentlstorms.virdirapptor.adapter;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.yentlstorms.virdirapptor.R;
import com.yentlstorms.virdirapptor.Utility;
import com.yentlstorms.virdirapptor.fragment.EventsFragment;
import com.yentlstorms.virdirapptor.fragment.FriendsFragment;

import java.io.File;
import java.net.URI;


/**
// * Created by Yentl on 5/11/2015.
// */

public class PersonAdapter extends CursorAdapter {

    public static class ViewHolder {
        public final ImageView iconView;
        public final TextView usernameView;
        public final TextView personnameView;

        public ViewHolder(View view) {
            iconView = (ImageView) view.findViewById(R.id.follows_list_item_icon);
            usernameView = (TextView) view.findViewById(R.id.follows_list_item_username);
            personnameView = (TextView) view.findViewById(R.id.follows_list_item_personname);
        }
    }

    public PersonAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        View view = LayoutInflater.from(context).inflate(R.layout.follows_list_item_small, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);

        return view;
    }

    public void bindView(View view, Context context, Cursor data) {

        if (data.getCount() < 1) {
            CharSequence text = "Could not find your userdata in database";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            return;
        }

        ViewHolder viewHolder = (ViewHolder) view.getTag();

        File profilePicture = new File(Utility.getProfilePicturesDirectory(context),
                data.getString(FriendsFragment.COL_PERSONS_PICTURE_URL));
        Log.v("PersonAdapter.java", " " + profilePicture.getAbsolutePath());
        //try {
        if ( profilePicture.exists() ) {
            Log.v("PersonAdapter.java", profilePicture.getAbsolutePath());
            viewHolder.iconView.setImageURI(Uri.fromFile(profilePicture));
        } else {
            viewHolder.iconView.setImageResource(R.drawable.user);
        }
        //}
        /*catch (Exception e) {
            Log.v("PersonAdapter.java", e.toString());
            viewHolder.iconView.setImageResource(R.drawable.user);
        }*/

        Log.v("PersonAdapter.java", data.getString(FriendsFragment.COL_PERSONS_ID) + " --- " +
                        data.getString(FriendsFragment.COL_PERSONS_USERNAME) + " --- " +
                        data.getString(FriendsFragment.COL_PERSONS_PERSONNAME) + " --- " +
                        data.getString(FriendsFragment.COL_PERSONS_DOB) + " --- " +
                        data.getString(FriendsFragment.COL_PERSONS_LOCATION_COUNTRY) + " --- " +
                        data.getString(FriendsFragment.COL_PERSONS_PICTURE_URL)
        );

        // Read data from cursor and display it on the correct Views.
        String username = data.getString(FriendsFragment.COL_PERSONS_USERNAME);
        viewHolder.usernameView.setText(username);

        String personname = data.getString(FriendsFragment.COL_PERSONS_PERSONNAME);
        viewHolder.personnameView.setText(personname);
    }

}

