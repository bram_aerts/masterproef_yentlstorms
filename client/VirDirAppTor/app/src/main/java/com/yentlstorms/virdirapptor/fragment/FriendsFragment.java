package com.yentlstorms.virdirapptor.fragment;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.support.v4.app.Fragment;
import android.widget.AdapterView;
import android.widget.ListView;

import com.yentlstorms.virdirapptor.R;
import com.yentlstorms.virdirapptor.adapter.PersonAdapter;
import com.yentlstorms.virdirapptor.bottomsheet.PersonBottomSheetFragment;
import com.yentlstorms.virdirapptor.data.DataContract;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FriendsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FriendsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FriendsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private PersonAdapter mPersonAdapter;
    private ListView mListView;

    private OnFragmentInteractionListener mListener;

    private static final int FRIENDSFRAGMENT_LOADER = 1;

    private static final String[] FRIENDSFRAGMENT_COLUMNS = {
            DataContract.PersonsEntry.TABLE_NAME + "." + DataContract.PersonsEntry._ID,
            DataContract.PersonsEntry.COLUMN_USERNAME,
            DataContract.PersonsEntry.COLUMN_PERSONNAME,
            DataContract.PersonsEntry.COLUMN_DOB,
            DataContract.PersonsEntry.COLUMN_LOCATION_COUNTRY,
            DataContract.PersonsEntry.COLUMN_PICTURE_URL,
    };

    public static final int COL_PERSONS_ID = 0;
    public static final int COL_PERSONS_USERNAME = 1;
    public static final int COL_PERSONS_PERSONNAME = 2;
    public static final int COL_PERSONS_DOB = 3;
    public static final int COL_PERSONS_LOCATION_COUNTRY = 4;
    public static final int COL_PERSONS_PICTURE_URL = 5;


    public static FriendsFragment newInstance() {
        FriendsFragment fragment = new FriendsFragment();
        return fragment;
    }

    public FriendsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mPersonAdapter = new PersonAdapter(getActivity(), null, 0);

        View rootView = inflater.inflate(R.layout.fragment_friends, container, false);

        // Get a reference to the ListView, and attach this adapter to it.
        mListView = (ListView) rootView.findViewById(R.id.fragment_friends_listview);
        mListView.setAdapter(mPersonAdapter);

        // TODO: add setOnItemClickListener to the listview
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int clickedPersonId = ((Cursor) mListView.getItemAtPosition(position)).getInt(FriendsFragment.COL_PERSONS_ID);
                Log.v("FriendsFragment", "Clicked person with personid = " + clickedPersonId);

                PersonBottomSheetFragment personBSDF = PersonBottomSheetFragment.newInstance(clickedPersonId);
                personBSDF.show(getFragmentManager(), "BOTTOM_SHEET_PERSON");
            }
        });

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getLoaderManager().initLoader(FRIENDSFRAGMENT_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {

        //String sortOrder = WeatherContract.WeatherEntry.COLUMN_DATE + " ASC";

        // TODO: Currently use 1 as given id, later on change to user's id
        Uri followsWithIDUri = DataContract.FollowsEntry.buildFollowsUri(1);

        return new CursorLoader(getActivity(),
                followsWithIDUri,
                FRIENDSFRAGMENT_COLUMNS,    // projection[]
                null,                       // selection        done in the provider itself.
                null,                       // selectionArgs[]
                null                        // sort order
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mPersonAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mPersonAdapter.swapCursor(null);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
