package com.yentlstorms.virdirapptor.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.yentlstorms.virdirapptor.Constants;
import com.yentlstorms.virdirapptor.R;


/**
 * Created by Yentl on 23/11/2015.
 */
public class ChangeLocationFragment extends DialogFragment {

    EditText streetView;
    EditText numberView;
    EditText zipView;
    EditText townView;
    EditText countryView;

    public ChangeLocationFragment() {
        ;
    }

    public static ChangeLocationFragment newInstance() {
        ChangeLocationFragment frag = new ChangeLocationFragment();
        return frag;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_change_location, null);

        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setView(view);
        alert.setTitle(getArguments().getString("title"));

        streetView = (EditText) view.findViewById(R.id.fragment_change_location_street);
        numberView = (EditText) view.findViewById(R.id.fragment_change_location_number);
        zipView = (EditText) view.findViewById(R.id.fragment_change_location_zip);
        townView = (EditText) view.findViewById(R.id.fragment_change_location_town);
        countryView = (EditText) view.findViewById(R.id.fragment_change_location_country);

        Bundle locBundle = getArguments().getBundle("currentValue");
        if ( locBundle == null ) { locBundle = new Bundle(); }
        streetView.setText(locBundle.getString(Constants.BUNDLE_KEY_LOCATION_STREET));
        numberView.setText(String.valueOf(locBundle.getInt(Constants.BUNDLE_KEY_LOCATION_NUMBER)));
        zipView.setText(String.valueOf(locBundle.getInt(Constants.BUNDLE_KEY_LOCATION_ZIP)));
        townView.setText(locBundle.getString(Constants.BUNDLE_KEY_LOCATION_TOWN));
        countryView.setText(locBundle.getString(Constants.BUNDLE_KEY_LOCATION_COUNTRY));



        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Intent intent = new Intent();
                Bundle resultBundle = new Bundle();
                resultBundle.putString(Constants.BUNDLE_KEY_LOCATION_STREET, streetView.getText().toString());
                resultBundle.putInt(Constants.BUNDLE_KEY_LOCATION_NUMBER, Integer.parseInt(numberView.getText().toString()));
                resultBundle.putInt(Constants.BUNDLE_KEY_LOCATION_ZIP, Integer.parseInt(zipView.getText().toString()));
                resultBundle.putString(Constants.BUNDLE_KEY_LOCATION_TOWN, townView.getText().toString());
                resultBundle.putString(Constants.BUNDLE_KEY_LOCATION_COUNTRY, countryView.getText().toString());
                intent.putExtras(resultBundle);
                getTargetFragment().onActivityResult(getTargetRequestCode(), 1, intent);
                return;
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                return;
            }
        });

        return alert.create();
    }
}