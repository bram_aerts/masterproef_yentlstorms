package com.yentlstorms.virdirapptor.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Yentl on 4/11/2015.
 * Describes the client-side database.
 */
public class DataContract {

    // Content Authority uniquely identifies the content provider.
    public static final String CONTENT_AUTHORITY = "com.yentlstorms.virdirapptor";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    // Possible paths to be appended to the BASE_CONTENT_URI to fetch the right database data.
    public static final String PATH_PERSONS = "persons";
    public static final String PATH_EVENTS = "events";
    public static final String PATH_PERSONS_EVENTS = "persons_events";
    public static final String PATH_LABELS = "labels";
    public static final String PATH_EVENTS_LABELS = "events_labels";
    public static final String PATH_FOLLOWS = "follows";

    // Inner class that defines the table contents of the PERSONS table.
    // = a list of available users
    public static final class PersonsEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_PERSONS).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PERSONS;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PERSONS;


        public static final String TABLE_NAME = "persons";

        public static final String COLUMN_USERNAME = "username";
        public static final String COLUMN_PERSONNAME = "personname";
        public static final String COLUMN_DOB = "dob";
        public static final String COLUMN_LOCATION_STREET = "locationstreet";
        public static final String COLUMN_LOCATION_NUMBER = "locationnumber";
        public static final String COLUMN_LOCATION_POSTAL = "locationpostal";
        public static final String COLUMN_LOCATION_TOWN = "locationtown";
        public static final String COLUMN_LOCATION_COUNTRY = "locationcountry";
        public static final String COLUMN_PICTURE_URL = "pictureURL";
        public static final String COLUMN_EMAIL = "email";
        public static final String COLUMN_LAST_CHANGED = "lastchanged";

        public static Uri buildPersonsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static String getIDFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }

    // Inner class that defines the table contents of the EVENTS table.
    // = a list of available events
    public static final class EventsEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_EVENTS).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_EVENTS;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_EVENTS;


        public static final String TABLE_NAME = "events";

        public static final String COLUMN_EVENTNAME = "eventname";
        public static final String COLUMN_STARTDATE = "startdate";
        public static final String COLUMN_ENDDATE = "enddate";

        public static final String COLUMN_LOCATION_STREET = "locationstreet";
        public static final String COLUMN_LOCATION_NUMBER = "locationnumber";
        public static final String COLUMN_LOCATION_POSTAL = "locationpostal";
        public static final String COLUMN_LOCATION_TOWN = "locationtown";
        public static final String COLUMN_LOCATION_COUNTRY = "locationcountry";
        public static final String COLUMN_PICTURE_URL = "pictureurl";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_CREATOR_ID = "creatorid";
        public static final String COLUMN_LAST_CHANGED = "lastchanged";

        public static Uri buildEventsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static String getIDFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }

    // Inner class that defines the table contents of the PERSONS_EVENTS table
    // = which person goes to which events.
    public static final class PersonsEventsEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_PERSONS_EVENTS).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PERSONS_EVENTS;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PERSONS_EVENTS;

        public static final String TABLE_NAME = "persons_events";

        //public static final String COLUMN_PERSONID = "personid"; == the implementation of _ID
        public static final String COLUMN_EVENTID = "eventid";

        public static Uri buildPersonsEventsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static String getIDFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }

    // Inner class that defines the table contents of the LABELS table
    // = labels used to describe events.
    public static final class LabelsEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_LABELS).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LABELS;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LABELS;

        public static final String TABLE_NAME = "labels";

        //public static final String COLUMN_LABELID = "labelid"; == the implementation of _ID
        public static final String COLUMN_LABELNAME = "labelname";

        public static Uri buildLabelsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static String getIDFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }

    // Inner class that defines the table contents of the EVENTS_LABELS table
    // = which labels are used for a specific event
    public static final class EventsLabelsEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_EVENTS_LABELS).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_EVENTS_LABELS;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_EVENTS_LABELS;

        public static final String TABLE_NAME = "events_labels";

        //public static final String COLUMN_EVENTID = "eventid"; == the implementation of _ID
        public static final String COLUMN_LABELID = "labelid";

        public static Uri buildEventsLabelsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static String getIDFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }


    public static final class FollowsEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_FOLLOWS).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_FOLLOWS;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_FOLLOWS;

        public static final String TABLE_NAME = "follows";

        //public static final String COLUMN_PERSONONE_ID = "personidone"; == the implementation of _ID
        public static final String COLUMN_PERSONTWO_ID = "personidtwo";

        public static Uri buildFollowsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static String getIDFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }


}
