package com.yentlstorms.virdirapptor.data;

import android.annotation.TargetApi;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

public class DataProvider extends ContentProvider {

    // The URI Matcher used by this content provider.
    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private DataDBHelper mOpenHelper;

    // PERSONS_WITH_ID: fetch the personal data of the person with given ID. (PERSONS Table data).
    static final int PERSONS = 100;
    static final int PERSONS_WITH_ID = 101;

    static final int PERSONS_EVENTS = 200;
    static final int PERSONS_EVENTS_WITH_ID = 201;

    static final int EVENTS = 300;
    static final int EVENTS_WITH_ID = 301;

    static final int LABELS = 400;
    static final int LABELS_WITH_ID = 401;

    static final int EVENTS_LABELS = 500;
    static final int EVENTS_LABELS_WITH_ID = 501;

    static final int FOLLOWS = 600;
    static final int FOLLOWS_WITH_ID = 601;

    private static final SQLiteQueryBuilder sEventsByIDQueryBuilder;
    private static final SQLiteQueryBuilder sEventsByPersonIDQueryBuilder;
    private static final SQLiteQueryBuilder sFollowsByIDQueryBuilder;


    static{
        sFollowsByIDQueryBuilder = new SQLiteQueryBuilder();
        //This is an inner join which looks like
        //follows INNER JOIN persons ON follows.persontwoid = persons._ID
        sFollowsByIDQueryBuilder.setTables(
                DataContract.FollowsEntry.TABLE_NAME + " INNER JOIN " +
                        DataContract.PersonsEntry.TABLE_NAME +
                        " ON " +
                        DataContract.FollowsEntry.TABLE_NAME + "." + DataContract.FollowsEntry.COLUMN_PERSONTWO_ID +
                        " = " +
                        DataContract.PersonsEntry.TABLE_NAME + "." + DataContract.PersonsEntry._ID
        );

        sEventsByPersonIDQueryBuilder = new SQLiteQueryBuilder();
        //This is an inner join which looks like
        //persons_events INNER JOIN events ON persons_events.eventid = events._ID
        sEventsByPersonIDQueryBuilder.setTables(
                DataContract.PersonsEventsEntry.TABLE_NAME + " INNER JOIN " +
                        DataContract.EventsEntry.TABLE_NAME +
                        " ON " +
                        DataContract.PersonsEventsEntry.TABLE_NAME + "." + DataContract.PersonsEventsEntry.COLUMN_EVENTID +
                        " = " +
                        DataContract.EventsEntry.TABLE_NAME + "." + DataContract.EventsEntry._ID
        );

        sEventsByIDQueryBuilder = new SQLiteQueryBuilder();


    }

    // persons_events._ID = ?
    private static final String sPersonsEventsByPersonIDSelection =
            DataContract.PersonsEventsEntry.TABLE_NAME +
                    "." + DataContract.PersonsEventsEntry._ID + " = ? ";

    // persons._ID = ?
    private static final String sPersonsByIDSelection =
            DataContract.PersonsEntry.TABLE_NAME +
                    "." + DataContract.PersonsEntry._ID + " = ? ";

    // events._ID = ?
    private static final String sEventsByIDSelection =
            DataContract.EventsEntry.TABLE_NAME +
                    "." + DataContract.EventsEntry._ID + " = ? ";

    // labels._ID = ?
    private static final String sLabelsByIDSelection =
            DataContract.LabelsEntry.TABLE_NAME +
                    "." + DataContract.LabelsEntry._ID + " = ? ";

    // events_labels._ID = ?
    private static final String sEventsLabelsByEventIDSelection =
            DataContract.EventsLabelsEntry.TABLE_NAME +
                    "." + DataContract.EventsLabelsEntry._ID + " = ? ";

    // follows._ID = ?
    private static final String sFollowsByPersonIDSelection =
            DataContract.FollowsEntry.TABLE_NAME +
                    "." + DataContract.FollowsEntry._ID + " = ? ";

    private Cursor getPersonsByID(Uri uri , String[] projection, String sortOrder) {
        String id = DataContract.PersonsEntry.getIDFromUri(uri);
        return mOpenHelper.getReadableDatabase().query(
                DataContract.PersonsEntry.TABLE_NAME,
                projection,
                sPersonsByIDSelection,
                new String[]{id},
                null,
                null,
                sortOrder
        );
    }

    private Cursor getEventsByID(Uri uri, String[] projection, String sortOrder) {
        String id = DataContract.EventsEntry.getIDFromUri(uri);
        return mOpenHelper.getReadableDatabase().query(
                DataContract.EventsEntry.TABLE_NAME,
                projection,
                sEventsByIDSelection,
                new String[]{id},
                null,
                null,
                sortOrder
        );
    }

    private Cursor getLabelsByID(Uri uri, String[] projection, String sortOrder) {
        String id = DataContract.LabelsEntry.getIDFromUri(uri);
        return mOpenHelper.getReadableDatabase().query(
                DataContract.LabelsEntry.TABLE_NAME,
                projection,
                sEventsByIDSelection,
                new String[]{id},
                null,
                null,
                sortOrder
        );
    }

    private Cursor getEventsLabelsByEventID(Uri uri, String[] projection, String sortOrder) {
        String id = DataContract.LabelsEntry.getIDFromUri(uri);
        return mOpenHelper.getReadableDatabase().query(
                DataContract.EventsLabelsEntry.TABLE_NAME,
                projection,
                sEventsLabelsByEventIDSelection,
                new String[]{id},
                null,
                null,
                sortOrder
        );
    }

    private Cursor getFollowsByID(Uri uri, String[] projection, String sortOrder) {
        String id = DataContract.FollowsEntry.getIDFromUri(uri);
        return sFollowsByIDQueryBuilder.query(
                mOpenHelper.getReadableDatabase(),
                projection,
                sFollowsByPersonIDSelection,
                new String[]{id},
                null,
                null,
                sortOrder
        );
    }

    private Cursor getEventsByPersonID(Uri uri, String[] projection, String sortOrder) {
        String id = DataContract.PersonsEventsEntry.getIDFromUri(uri);
        return sEventsByPersonIDQueryBuilder.query(
                mOpenHelper.getReadableDatabase(),
                projection,
                sPersonsEventsByPersonIDSelection,
                new String[]{id},
                null,
                null,
                sortOrder
        );
    }

    static UriMatcher buildUriMatcher() {
        // found.  The code passed into the constructor represents the code to return for the root
        // URI.  It's common to use NO_MATCH as the code for this case.
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = DataContract.CONTENT_AUTHORITY;

        // For each type of URI you want to add, create a corresponding code.
        //      * matches any text
        //      # matches only numbers
        matcher.addURI(authority, DataContract.PATH_PERSONS, PERSONS);
        matcher.addURI(authority, DataContract.PATH_PERSONS + "/#", PERSONS_WITH_ID);

        matcher.addURI(authority, DataContract.PATH_PERSONS_EVENTS, PERSONS_EVENTS);
        matcher.addURI(authority, DataContract.PATH_PERSONS_EVENTS + "/#", PERSONS_EVENTS_WITH_ID);

        matcher.addURI(authority, DataContract.PATH_EVENTS, EVENTS);
        matcher.addURI(authority, DataContract.PATH_EVENTS + "/#", EVENTS_WITH_ID);

        matcher.addURI(authority, DataContract.PATH_LABELS, LABELS);
        matcher.addURI(authority, DataContract.PATH_LABELS + "/#", LABELS_WITH_ID);

        matcher.addURI(authority, DataContract.PATH_EVENTS_LABELS, EVENTS_LABELS);
        matcher.addURI(authority, DataContract.PATH_EVENTS_LABELS + "/#", EVENTS_LABELS_WITH_ID);

        matcher.addURI(authority, DataContract.PATH_FOLLOWS, FOLLOWS);
        matcher.addURI(authority, DataContract.PATH_FOLLOWS + "/#", FOLLOWS_WITH_ID);
        return matcher;
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new DataDBHelper(getContext());
        return true;
    }

    @Override
    public String getType(Uri uri) {

        // Use the Uri Matcher to determine what kind of URI this is.
        final int match = sUriMatcher.match(uri);

        switch (match) {
            case PERSONS :
                return DataContract.PersonsEntry.CONTENT_TYPE;
            case PERSONS_WITH_ID :
                return DataContract.PersonsEntry.CONTENT_ITEM_TYPE;
            case PERSONS_EVENTS :
                return DataContract.PersonsEventsEntry.CONTENT_TYPE;
            case PERSONS_EVENTS_WITH_ID :
                return DataContract.PersonsEventsEntry.CONTENT_TYPE;    // INNER JOIN --> POSSIBLY MULTIPLE ROWS
            case EVENTS :
                return DataContract.EventsEntry.CONTENT_TYPE;
            case EVENTS_WITH_ID :
                return DataContract.EventsEntry.CONTENT_ITEM_TYPE;
            case LABELS :
                return DataContract.LabelsEntry.CONTENT_TYPE;
            case LABELS_WITH_ID :
                return DataContract.LabelsEntry.CONTENT_ITEM_TYPE;
            case EVENTS_LABELS :
                return DataContract.EventsLabelsEntry.CONTENT_TYPE;
            case EVENTS_LABELS_WITH_ID :
                return DataContract.EventsLabelsEntry.CONTENT_TYPE; // Single event can have multiple labels
            case FOLLOWS :
                return DataContract.FollowsEntry.CONTENT_TYPE;
            case FOLLOWS_WITH_ID :
                return DataContract.FollowsEntry.CONTENT_TYPE;      // INNER JOIN --> POSSIBLY MULTIPLE ROWS
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        // Here's the switch statement that, given a URI, will determine what kind of request it is,
        // and query the database accordingly.
        Cursor retCursor;
        switch (sUriMatcher.match(uri)) {
            // "persons/#"
            case PERSONS_WITH_ID : {
                retCursor = getPersonsByID(uri, projection, sortOrder);
                break;
            }
            case PERSONS_EVENTS_WITH_ID : {
                retCursor = getEventsByPersonID(uri, projection, sortOrder);
                break;
            }
            case EVENTS_WITH_ID : {
                retCursor = getEventsByID(uri, projection, sortOrder);
                break;
            }
            case EVENTS_LABELS_WITH_ID : {
                retCursor = getEventsLabelsByEventID(uri, projection, sortOrder);
                break;
            }
            case FOLLOWS_WITH_ID : {
                retCursor = getFollowsByID(uri, projection, sortOrder);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }


    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case PERSONS : {
                long _id = db.insertWithOnConflict(DataContract.PersonsEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                if ( _id > 0 )
                    returnUri = DataContract.PersonsEntry.buildPersonsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case PERSONS_WITH_ID : {
                long _id = db.insertWithOnConflict(DataContract.PersonsEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                if ( _id > 0 )
                    returnUri = DataContract.PersonsEntry.buildPersonsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case PERSONS_EVENTS : {
                long _id = db.insertWithOnConflict(DataContract.PersonsEventsEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                if ( _id > 0 )
                    returnUri = DataContract.PersonsEventsEntry.buildPersonsEventsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case EVENTS : {
                long _id = db.insertWithOnConflict(DataContract.EventsEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                if ( _id > 0 )
                    returnUri = DataContract.EventsEntry.buildEventsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case EVENTS_WITH_ID : {
                long _id = db.insertWithOnConflict(DataContract.EventsEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                if ( _id > 0 )
                    returnUri = DataContract.EventsEntry.buildEventsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case LABELS : {
                long _id = db.insertWithOnConflict(DataContract.LabelsEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                if ( _id > 0 )
                    returnUri = DataContract.LabelsEntry.buildLabelsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case EVENTS_LABELS : {
                long _id = db.insertWithOnConflict(DataContract.EventsLabelsEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                if ( _id > 0 )
                    returnUri = DataContract.EventsLabelsEntry.buildEventsLabelsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case FOLLOWS : {
                Log.v("DataProvider", sFollowsByIDQueryBuilder.getTables());
                long _id = db.insertWithOnConflict(DataContract.FollowsEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                if ( _id > 0 )
                    returnUri = DataContract.PersonsEntry.buildPersonsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsDeleted;
        // this makes delete all rows return the number of rows deleted
        if ( null == selection ) selection = "1";
        switch (match) {

            case PERSONS_WITH_ID:
                rowsDeleted = db.delete(
                        DataContract.PersonsEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Because a null deletes all rows
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(
            Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;

        switch (match) {
            case PERSONS :
                rowsUpdated = db.update(DataContract.PersonsEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case PERSONS_WITH_ID:
                rowsUpdated = db.update(DataContract.PersonsEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case PERSONS_EVENTS:
                rowsUpdated = db.update(DataContract.PersonsEventsEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case PERSONS_EVENTS_WITH_ID:
                rowsUpdated = db.update(DataContract.PersonsEventsEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case EVENTS :
                rowsUpdated = db.update(DataContract.EventsEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case EVENTS_WITH_ID :
                rowsUpdated = db.update(DataContract.EventsEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int returnCount = 0;
        switch (match) {
            case PERSONS :
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        long _id = db.insertWithOnConflict(DataContract.PersonsEntry.TABLE_NAME, null, value, SQLiteDatabase.CONFLICT_REPLACE);
                        if ( _id != -1 ) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            case PERSONS_EVENTS :
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        long _id = db.insertWithOnConflict(DataContract.PersonsEventsEntry.TABLE_NAME, null, value, SQLiteDatabase.CONFLICT_REPLACE);
                        if ( _id != -1 ) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            case EVENTS :
                db.beginTransaction();
                try {
                    for (ContentValues value: values) {
                        long _id = db.insertWithOnConflict(DataContract.EventsEntry.TABLE_NAME, null, value, SQLiteDatabase.CONFLICT_REPLACE);
                        if ( _id != -1 ) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            case LABELS :
                db.beginTransaction();
                try {
                    for (ContentValues value: values) {
                        long _id = db.insertWithOnConflict(DataContract.LabelsEntry.TABLE_NAME, null, value, SQLiteDatabase.CONFLICT_REPLACE);
                        if ( _id != -1 ) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            case EVENTS_LABELS :
                db.beginTransaction();
                try {
                    for (ContentValues value: values) {
                        long _id = db.insertWithOnConflict(DataContract.EventsLabelsEntry.TABLE_NAME, null, value, SQLiteDatabase.CONFLICT_REPLACE);
                        if ( _id != -1 ) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            default:
                return super.bulkInsert(uri, values);
        }
    }

    // You do not need to call this method. This is a method specifically to assist the testing
    // framework in running smoothly. You can read more at:
    // http://developer.android.com/reference/android/content/ContentProvider.html#shutdown()
    @Override
    @TargetApi(11)
    public void shutdown() {
        mOpenHelper.close();
        super.shutdown();
    }
}