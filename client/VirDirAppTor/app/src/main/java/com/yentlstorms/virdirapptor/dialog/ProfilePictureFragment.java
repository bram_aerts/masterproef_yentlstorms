package com.yentlstorms.virdirapptor.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.soundcloud.android.crop.Crop;
import com.yentlstorms.virdirapptor.Constants;
import com.yentlstorms.virdirapptor.R;
import com.yentlstorms.virdirapptor.Utility;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Yentl on 21/02/2016.
 */
public class ProfilePictureFragment extends DialogFragment implements View.OnClickListener {


    private final String LOG_TAG = ProfilePictureFragment.class.getSimpleName();

    Button newPictureButton;
    Button existingPictureButton;

    static final int REQUEST_IMAGE_CAPTURE = 9864;

    private File mUncroppedPictureFile;
    private File mCroppedPictureFile;

    public static ProfilePictureFragment newInstance() {
        ProfilePictureFragment frag = new ProfilePictureFragment();
        return frag;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_profile_picture, null);

        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setView(view);
        //alert.setTitle(getArguments().getString("title"));
        alert.setTitle("Profile Picture");

        newPictureButton = (Button) view.findViewById(R.id.fragment_profile_picture_new);
        existingPictureButton = (Button) view.findViewById(R.id.fragment_profile_picture_existing);

        newPictureButton.setOnClickListener(this);
        existingPictureButton.setOnClickListener(this);

        return alert.create();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_profile_picture_new:
                dispatchTakePictureIntent();
                break;
            case R.id.fragment_profile_picture_existing:
                Crop.pickImage(getContext(), getParentFragment());
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.v(LOG_TAG, "ActivityResult: " + requestCode + " -- " + resultCode);

        switch (requestCode) {
            case REQUEST_IMAGE_CAPTURE:
                if (resultCode == Activity.RESULT_OK) {
                    Log.v(LOG_TAG, "image taken");
                    if (mUncroppedPictureFile != null) {
                        // Photo taken, must now be cropped to square
                        dispatchCropPicture();
                    }
                }
                break;
            case Crop.REQUEST_CROP:
                if (resultCode == Activity.RESULT_OK) {
                    Log.v(LOG_TAG, "image cropped OK");
                    // Now we must return the cropped picture to the original activity
                    Log.v(LOG_TAG, "cropped Picture filename = " + mCroppedPictureFile.getName());
                    Intent intent = new Intent();
                    intent.putExtra(Constants.BUNDLE_KEY_CROPPED_PICTURE, mCroppedPictureFile.getName());
                    getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, intent);
                    this.dismissAllowingStateLoss();
                }
                break;
            case Crop.REQUEST_PICK:
                if (resultCode == Activity.RESULT_OK) {
                    Log.v(LOG_TAG, "image picked");
                    Uri inputUri = data.getData();
                    if (inputUri.getScheme().equals("content")) {
                        dispatchCropPicture(inputUri);
                    } else if (inputUri.getScheme().equals("file")) {
                        mUncroppedPictureFile = new File(inputUri.getPath());
                        dispatchCropPicture();
                    }
                }
        }
    }

    private void dispatchTakePictureIntent() {
        Log.v(LOG_TAG, "dispatchTakenPictureIntent");

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure there's a camera intent, able to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create file where photo should be stored
            try {
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String imageFileName = "JPEG_" + timeStamp + "_";
                // Cannot use cache, as image being taken is done by camera
                // which cannot access the cache memory for this particular application
                // so we create a temporary file, and delete this afterwards.
                mUncroppedPictureFile = File.createTempFile(
                        imageFileName,
                        ".jpg",
                        getContext().getExternalFilesDir(null)
                );
                if (mUncroppedPictureFile != null) {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            Uri.fromFile(mUncroppedPictureFile));
                    getTargetFragment().startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            } catch (IOException ioe) {
                Log.v(LOG_TAG, "IOException " + ioe);
                return;
            }
        }
    }

    private void dispatchCropPicture() {
        Log.v(LOG_TAG, "dispatchCropPicture");
        try {
            // TODO: change to actual ID
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            mCroppedPictureFile = new File(Utility.getProfilePicturesDirectory(getContext()),
                    "1" + "_" + timeStamp + ".jpg");
            if (mCroppedPictureFile.createNewFile()) {
                // File already exists: first delete and recreate
                Log.v(LOG_TAG, "file existed, recreating");
                mCroppedPictureFile.delete();
                mCroppedPictureFile.createNewFile();
            }
            Crop.of(Uri.fromFile(mUncroppedPictureFile), Uri.fromFile(mCroppedPictureFile)).asSquare().start(getContext(), getParentFragment());

            //mUncroppedPictureFile.delete();
        } catch (IOException ioe) {
            Log.v(LOG_TAG, "IOException on creating mCroppedPictureUri");
            return;
        }
    }

    private void dispatchCropPicture(Uri source) {
        Log.v(LOG_TAG, "dispatchCropPicture");
        try {
            // TODO: change to actual ID
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            mCroppedPictureFile = new File(Utility.getProfilePicturesDirectory(getContext()),
                    "1" + "_" + timeStamp + ".jpg");
            if (mCroppedPictureFile.createNewFile()) {
                // File already exists: first delete and recreate
                Log.v(LOG_TAG, "file existed, recreating");
                mCroppedPictureFile.delete();
                mCroppedPictureFile.createNewFile();
            }
            Crop.of(source, Uri.fromFile(mCroppedPictureFile)).asSquare().start(getContext(), getParentFragment());

            //mUncroppedPictureFile.delete();
        } catch (IOException ioe) {
            Log.v(LOG_TAG, "IOException on creating mCroppedPictureUri");
            return;
        }
    }



}
