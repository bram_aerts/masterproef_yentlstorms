package com.yentlstorms.virdirapptor.dialog;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.yentlstorms.virdirapptor.R;

import java.util.Calendar;

/**
 * Created by Yentl on 19/11/2015.
 */
public class SingleInputFragment extends DialogFragment {

    public SingleInputFragment() {
        ;
    }

    public static SingleInputFragment newInstance() {
        SingleInputFragment frag = new SingleInputFragment();
        return frag;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_single_input, null);

        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle(getArguments().getString("title"));
        //alert.setMessage("Message :");

        // Set an EditText view to get user input
        final EditText inputField = new EditText(getContext());
        inputField.setHint(getArguments().getCharSequence("currentValue"));
        inputField.setText(getArguments().getCharSequence("currentValue"));
        alert.setView(inputField);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Intent intent = new Intent();
                intent.putExtra("returnValue", inputField.getText().toString());
                getTargetFragment().onActivityResult(getTargetRequestCode(), 1, intent);
                return;
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                return;
            }
        });

        return alert.create();
    }
}