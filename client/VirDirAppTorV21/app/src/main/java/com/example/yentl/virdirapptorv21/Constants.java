package com.example.yentl.virdirapptorv21;

import java.io.File;

/**
 * Created by Yentl on 24/11/2015.
 * Class that contains a bunch of public static final resources,
 * that are either not specific for a certain class, or can be used in multiple classes.
 */
public final class Constants {

    // Local network server: changed on different development machines.
    private static String server_ip = "192.168.0.122";
    //private String server_ip = "10.68.251.234";
    private static String server_port = "8080";
    public static final String SERVER_BASE_URL = "http://" + server_ip + ":" + server_port +
            File.separator + "VirDirAppTor-war" + File.separator;

    public static final File PROFILE_PICTURES_DIR = new File("");


    public static final String BUNDLE_KEY_PERSONID = "personid";                //INT
    public static final String BUNDLE_KEY_IMAGE = "imageResource";              //INT
    public static final String BUNDLE_KEY_USERNAME = "userName";                //STRING
    public static final String BUNDLE_KEY_PERSONNAME = "personName";            //STRING
    public static final String BUNDLE_KEY_DOB = "dob";                          //BUNDLE
    public static final String BUNDLE_KEY_DATE_DAY = "dateDay";                 //INT
    public static final String BUNDLE_KEY_DATE_MONTH = "dateMonth";             //INT
    public static final String BUNDLE_KEY_DATE_YEAR = "dateYear";               //INT

    public static final String BUNDLE_KEY_EMAIL = "email";                      //STRING
    // Location is a bundle on itself, containing the street, number, zip, town and country
    public static final String BUNDLE_KEY_LOCATION = "location";                //BUNDLE
    public static final String BUNDLE_KEY_LOCATION_STREET = "locationStreet";   //STRING
    public static final String BUNDLE_KEY_LOCATION_NUMBER = "locationNumber";   //INT
    public static final String BUNDLE_KEY_LOCATION_ZIP = "locationZip";         //INT
    public static final String BUNDLE_KEY_LOCATION_TOWN = "locationTown";       //STRING
    public static final String BUNDLE_KEY_LOCATION_COUNTRY = "locationCountry"; //STRING

    public static final String BUNDLE_KEY_UNCROPPED_PICTURE = "uncroppedPicture"; //STRING
    public static final String BUNDLE_KEY_CROPPED_PICTURE = "croppedPicture";   //STRING

    public static final String BUNDLE_KEY_LASTCHANGED = "lastchanged";           //INT



    public static final String JSON_KEY_PERSONID = "personid";
    public static final String JSON_KEY_USERNAME = "username";
    public static final String JSON_KEY_PERSONNAME = "personname";
    public static final String JSON_KEY_DOB = "dob";
    public static final String JSON_KEY_LOCATIONSTREET = "locationstreet";
    public static final String JSON_KEY_LOCATIONNUMBER = "locationnumber";
    public static final String JSON_KEY_LOCATIONPOSTAL = "locationpostal";
    public static final String JSON_KEY_LOCATIONTOWN = "locationtown";
    public static final String JSON_KEY_LOCATIONCOUNTRY = "locationcountry";
    public static final String JSON_KEY_PICTUREURL = "pictureurl";
    public static final String JSON_KEY_EMAIL = "email";
    public static final String JSON_KEY_LASTCHANGED = "lastchanged";
    public static final String JSON_KEY_FOLLOWS_LIST = "follows";


    public static final String DIR_PROFILE_PICTURES = "ProfilePictures";

    public static final String lineEnd = "\r\n";
    public static final String twoHyphens = "--";
    public static final String boundary =  "*****";
}
