package com.example.yentl.virdirapptorv21.data;

import android.annotation.TargetApi;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

public class DataProvider extends ContentProvider {

    // The URI Matcher used by this content provider.
    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private DataDBHelper mOpenHelper;

    // PERSONS_WITH_ID: fetch the personal data of the person with given ID. (PERSONS Table data).
    static final int PERSONS = 100;
    static final int PERSONS_WITH_ID = 101;
    //static final int WEATHER_WITH_LOCATION = 101;
    //static final int WEATHER_WITH_LOCATION_AND_DATE = 102;
    //static final int LOCATION = 300;

    static final int FOLLOWS = 300;
    static final int FOLLOWS_WITH_ID = 301;

    private static final SQLiteQueryBuilder sFollowsByIDQueryBuilder;

    static{
        sFollowsByIDQueryBuilder = new SQLiteQueryBuilder();

        //This is an inner join which looks like
        //follows INNER JOIN persons ON follows.persontwoid = persons._ID
        sFollowsByIDQueryBuilder.setTables(
                DataContract.FollowsEntry.TABLE_NAME + " INNER JOIN " +
                        DataContract.PersonsEntry.TABLE_NAME +
                        " ON " +
                        DataContract.FollowsEntry.TABLE_NAME + "." + DataContract.FollowsEntry.COLUMN_PERSONTWO_ID +
                        " = " +
                        DataContract.PersonsEntry.TABLE_NAME + "." + DataContract.PersonsEntry._ID
        );
    }

    // persons._ID = ?
    private static final String sPersonsIDSelection =
            DataContract.PersonsEntry.TABLE_NAME +
                    "." + DataContract.PersonsEntry._ID + " = ? ";

    // follows._ID = ?
    private static final String sFollowsByIDSelection =
            DataContract.FollowsEntry.TABLE_NAME +
                    "." + DataContract.FollowsEntry._ID + " = ? ";

    private Cursor getFollowsByID(Uri uri, String[] projection, String sortOrder) {
        String id = DataContract.FollowsEntry.getIDFromUri(uri);
        return sFollowsByIDQueryBuilder.query(mOpenHelper.getReadableDatabase(),
                projection,
                sFollowsByIDSelection,
                new String[]{id},
                null,
                null,
                sortOrder
        );
    }

    /*
        Students: Here is where you need to create the UriMatcher. This UriMatcher will
        match each URI to the WEATHER, WEATHER_WITH_LOCATION, WEATHER_WITH_LOCATION_AND_DATE,
        and LOCATION integer constants defined above.  You can test this by uncommenting the
        testUriMatcher test within TestUriMatcher.
     */
    static UriMatcher buildUriMatcher() {
        // I know what you're thinking.  Why create a UriMatcher when you can use regular
        // expressions instead?  Because you're not crazy, that's why.

        // All paths added to the UriMatcher have a corresponding code to return when a match is
        // found.  The code passed into the constructor represents the code to return for the root
        // URI.  It's common to use NO_MATCH as the code for this case.
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = DataContract.CONTENT_AUTHORITY;

        // For each type of URI you want to add, create a corresponding code.
        //      * matches any text
        //      # matches only numbers
        matcher.addURI(authority, DataContract.PATH_PERSONS, PERSONS);
        matcher.addURI(authority, DataContract.PATH_PERSONS + "/#", PERSONS_WITH_ID);

        matcher.addURI(authority, DataContract.PATH_FOLLOWS, FOLLOWS);
        matcher.addURI(authority, DataContract.PATH_FOLLOWS + "/#", FOLLOWS_WITH_ID);
        return matcher;
    }

    /*
        Students: We've coded this for you.  We just create a new WeatherDbHelper for later use
        here.
     */
    @Override
    public boolean onCreate() {
        mOpenHelper = new DataDBHelper(getContext());
        return true;
    }

    /*
        Students: Here's where you'll code the getType function that uses the UriMatcher.  You can
        test this by uncommenting testGetType in TestProvider.

     */
    @Override
    public String getType(Uri uri) {

        // Use the Uri Matcher to determine what kind of URI this is.
        final int match = sUriMatcher.match(uri);

        switch (match) {
            // Student: Uncomment and fill out these two cases
            case PERSONS :
                return DataContract.PersonsEntry.CONTENT_TYPE;
            case PERSONS_WITH_ID :
                return DataContract.PersonsEntry.CONTENT_ITEM_TYPE;
            case FOLLOWS :
                return DataContract.FollowsEntry.CONTENT_TYPE;
            case FOLLOWS_WITH_ID :
                return DataContract.FollowsEntry.CONTENT_TYPE;      // INNER JOIN --> POSSIBLE MULTIPLE ROWS
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        // Here's the switch statement that, given a URI, will determine what kind of request it is,
        // and query the database accordingly.
        Cursor retCursor;
        switch (sUriMatcher.match(uri)) {
            // "persons/#"
            case PERSONS_WITH_ID : {
                String id = DataContract.PersonsEntry.getIDFromUri(uri);
                retCursor = mOpenHelper.getReadableDatabase().query(
                        DataContract.PersonsEntry.TABLE_NAME,
                        projection,
                        sPersonsIDSelection,
                        new String[]{id},
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case FOLLOWS_WITH_ID : {
                retCursor = getFollowsByID(uri, projection, sortOrder);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    /*
        Student: Add the ability to insert Locations to the implementation of this function.
     */
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case PERSONS : {
                long _id = db.insertWithOnConflict(DataContract.PersonsEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                if ( _id > 0 )
                    returnUri = DataContract.PersonsEntry.buildPersonsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case PERSONS_WITH_ID : {
                long _id = db.insertWithOnConflict(DataContract.PersonsEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                if ( _id > 0 )
                    returnUri = DataContract.PersonsEntry.buildPersonsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case FOLLOWS : {
                Log.v("DataProvider", sFollowsByIDQueryBuilder.getTables());
                long _id = db.insertWithOnConflict(DataContract.FollowsEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                if ( _id > 0 )
                    returnUri = DataContract.PersonsEntry.buildPersonsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsDeleted;
        // this makes delete all rows return the number of rows deleted
        if ( null == selection ) selection = "1";
        switch (match) {

            case PERSONS_WITH_ID:
                rowsDeleted = db.delete(
                        DataContract.PersonsEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Because a null deletes all rows
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(
            Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;

        switch (match) {
            case PERSONS : {
                rowsUpdated = db.update(DataContract.PersonsEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            }
            case PERSONS_WITH_ID:
                rowsUpdated = db.update(DataContract.PersonsEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int returnCount = 0;
        switch (match) {
            case PERSONS :
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        long _id = db.insertWithOnConflict(DataContract.PersonsEntry.TABLE_NAME, null, value, SQLiteDatabase.CONFLICT_REPLACE);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            default:
                return super.bulkInsert(uri, values);
        }
    }

    // You do not need to call this method. This is a method specifically to assist the testing
    // framework in running smoothly. You can read more at:
    // http://developer.android.com/reference/android/content/ContentProvider.html#shutdown()
    @Override
    @TargetApi(11)
    public void shutdown() {
        mOpenHelper.close();
        super.shutdown();
    }
}