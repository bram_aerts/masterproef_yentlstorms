/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbData;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Yentl
 */
@Entity
@Table(name = "PERSONS")
@NamedQueries({
    @NamedQuery(name = "Persons.findAll", query = "SELECT p FROM Persons p"),
    @NamedQuery(name = "Persons.findByPersonid", query = "SELECT p FROM Persons p WHERE p.personid = :personid"),
    @NamedQuery(name = "Persons.findByUsername", query = "SELECT p FROM Persons p WHERE p.username = :username"),
    @NamedQuery(name = "Persons.findByPersonname", query = "SELECT p FROM Persons p WHERE p.personname = :personname"),
    @NamedQuery(name = "Persons.findByDob", query = "SELECT p FROM Persons p WHERE p.dob = :dob"),
    @NamedQuery(name = "Persons.findByLocationstreet", query = "SELECT p FROM Persons p WHERE p.locationstreet = :locationstreet"),
    @NamedQuery(name = "Persons.findByLocationnumber", query = "SELECT p FROM Persons p WHERE p.locationnumber = :locationnumber"),
    @NamedQuery(name = "Persons.findByLocationpostal", query = "SELECT p FROM Persons p WHERE p.locationpostal = :locationpostal"),
    @NamedQuery(name = "Persons.findByLocationtown", query = "SELECT p FROM Persons p WHERE p.locationtown = :locationtown"),
    @NamedQuery(name = "Persons.findByLocationcountry", query = "SELECT p FROM Persons p WHERE p.locationcountry = :locationcountry"),
    @NamedQuery(name = "Persons.findByPictureurl", query = "SELECT p FROM Persons p WHERE p.pictureurl = :pictureurl"),
    @NamedQuery(name = "Persons.findByEmail", query = "SELECT p FROM Persons p WHERE p.email = :email"),
    @NamedQuery(name = "Persons.findByPassword", query = "SELECT p FROM Persons p WHERE p.password = :password")})
public class Persons implements Serializable {

    @Column(name = "LASTCHANGED")
    private BigInteger lastchanged;
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PERSONID")
    private BigDecimal personid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "USERNAME")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "PERSONNAME")
    private String personname;
    @Column(name = "DOB")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dob;
    @Size(max = 50)
    @Column(name = "LOCATIONSTREET")
    private String locationstreet;
    @Column(name = "LOCATIONNUMBER")
    private BigInteger locationnumber;
    @Column(name = "LOCATIONPOSTAL")
    private BigInteger locationpostal;
    @Size(max = 50)
    @Column(name = "LOCATIONTOWN")
    private String locationtown;
    @Size(max = 10)
    @Column(name = "LOCATIONCOUNTRY")
    private String locationcountry;
    @Size(max = 100)
    @Column(name = "PICTUREURL")
    private String pictureurl;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "EMAIL")
    private String email;
    @Size(max = 100)
    @Column(name = "PASSWORD")
    private String password;
    @ManyToMany(mappedBy = "personsCollection")
    private Collection<Events> eventsCollection;
    @JoinTable(name = "FOLLOWS", joinColumns = {
        @JoinColumn(name = "PERSONIDONE", referencedColumnName = "PERSONID")}, inverseJoinColumns = {
        @JoinColumn(name = "PERSONIDTWO", referencedColumnName = "PERSONID")})
    @ManyToMany
    private Collection<Persons> personsCollection;
    @ManyToMany(mappedBy = "personsCollection")
    private Collection<Persons> personsCollection1;

    public Persons() {
    }

    public Persons(BigDecimal personid) {
        this.personid = personid;
    }

    public Persons(BigDecimal personid, String username, String personname) {
        this.personid = personid;
        this.username = username;
        this.personname = personname;
    }

    public BigDecimal getPersonid() {
        return personid;
    }

    public void setPersonid(BigDecimal personid) {
        this.personid = personid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPersonname() {
        return personname;
    }

    public void setPersonname(String personname) {
        this.personname = personname;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getLocationstreet() {
        return locationstreet;
    }

    public void setLocationstreet(String locationstreet) {
        this.locationstreet = locationstreet;
    }

    public BigInteger getLocationnumber() {
        return locationnumber;
    }

    public void setLocationnumber(BigInteger locationnumber) {
        this.locationnumber = locationnumber;
    }

    public BigInteger getLocationpostal() {
        return locationpostal;
    }

    public void setLocationpostal(BigInteger locationpostal) {
        this.locationpostal = locationpostal;
    }

    public String getLocationtown() {
        return locationtown;
    }

    public void setLocationtown(String locationtown) {
        this.locationtown = locationtown;
    }

    public String getLocationcountry() {
        return locationcountry;
    }

    public void setLocationcountry(String locationcountry) {
        this.locationcountry = locationcountry;
    }

    public String getPictureurl() {
        return pictureurl;
    }

    public void setPictureurl(String pictureurl) {
        this.pictureurl = pictureurl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<Events> getEventsCollection() {
        return eventsCollection;
    }

    public void setEventsCollection(Collection<Events> eventsCollection) {
        this.eventsCollection = eventsCollection;
    }

    public Collection<Persons> getPersonsCollection() {
        return personsCollection;
    }

    public void setPersonsCollection(Collection<Persons> personsCollection) {
        this.personsCollection = personsCollection;
    }

    public Collection<Persons> getPersonsCollection1() {
        return personsCollection1;
    }

    public void setPersonsCollection1(Collection<Persons> personsCollection1) {
        this.personsCollection1 = personsCollection1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personid != null ? personid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Persons)) {
            return false;
        }
        Persons other = (Persons) object;
        if ((this.personid == null && other.personid != null) || (this.personid != null && !this.personid.equals(other.personid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbData.Persons[ personid=" + personid + " ]";
    }

    public BigInteger getLastchanged() {
        return lastchanged;
    }

    public void setLastchanged(BigInteger lastchanged) {
        this.lastchanged = lastchanged;
    }
    
}
