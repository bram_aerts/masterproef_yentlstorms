/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbData;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Yentl
 */
@Entity
@Table(name = "EVENTS")
@NamedQueries({
    @NamedQuery(name = "Events.findAll", query = "SELECT e FROM Events e"),
    @NamedQuery(name = "Events.findByEventid", query = "SELECT e FROM Events e WHERE e.eventid = :eventid"),
    @NamedQuery(name = "Events.findByEventname", query = "SELECT e FROM Events e WHERE e.eventname = :eventname"),
    @NamedQuery(name = "Events.findByStartdate", query = "SELECT e FROM Events e WHERE e.startdate = :startdate"),
    @NamedQuery(name = "Events.findByEnddate", query = "SELECT e FROM Events e WHERE e.enddate = :enddate"),
    @NamedQuery(name = "Events.findByLocationstreet", query = "SELECT e FROM Events e WHERE e.locationstreet = :locationstreet"),
    @NamedQuery(name = "Events.findByLocationnumber", query = "SELECT e FROM Events e WHERE e.locationnumber = :locationnumber"),
    @NamedQuery(name = "Events.findByLocationpostal", query = "SELECT e FROM Events e WHERE e.locationpostal = :locationpostal"),
    @NamedQuery(name = "Events.findByLocationtown", query = "SELECT e FROM Events e WHERE e.locationtown = :locationtown"),
    @NamedQuery(name = "Events.findByLocationcountry", query = "SELECT e FROM Events e WHERE e.locationcountry = :locationcountry"),
    @NamedQuery(name = "Events.findByPictureurl", query = "SELECT e FROM Events e WHERE e.pictureurl = :pictureurl"),
    @NamedQuery(name = "Events.findByDescription", query = "SELECT e FROM Events e WHERE e.description = :description")})
public class Events implements Serializable {

    @Column(name = "CREATORID")
    private BigInteger creatorid;
    @Column(name = "LASTCHANGED")
    private BigInteger lastchanged;
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "EVENTID")
    private BigDecimal eventid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "EVENTNAME")
    private String eventname;
    @Column(name = "STARTDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startdate;
    @Column(name = "ENDDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enddate;
    @Size(max = 50)
    @Column(name = "LOCATIONSTREET")
    private String locationstreet;
    @Column(name = "LOCATIONNUMBER")
    private BigInteger locationnumber;
    @Column(name = "LOCATIONPOSTAL")
    private BigInteger locationpostal;
    @Size(max = 50)
    @Column(name = "LOCATIONTOWN")
    private String locationtown;
    @Size(max = 10)
    @Column(name = "LOCATIONCOUNTRY")
    private String locationcountry;
    @Size(max = 100)
    @Column(name = "PICTUREURL")
    private String pictureurl;
    @Size(max = 200)
    @Column(name = "DESCRIPTION")
    private String description;
    @JoinTable(name = "PERSONS_EVENTS", joinColumns = {
        @JoinColumn(name = "EVENTID", referencedColumnName = "EVENTID")}, inverseJoinColumns = {
        @JoinColumn(name = "PERSONID", referencedColumnName = "PERSONID")})
    @ManyToMany
    private Collection<Persons> personsCollection;
    @JoinTable(name = "EVENTS_LABELS", joinColumns = {
        @JoinColumn(name = "EVENTID", referencedColumnName = "EVENTID")}, inverseJoinColumns = {
        @JoinColumn(name = "LABELID", referencedColumnName = "LABELID")})
    @ManyToMany
    private Collection<Labels> labelsCollection;

    public Events() {
    }

    public Events(BigDecimal eventid) {
        this.eventid = eventid;
    }

    public Events(BigDecimal eventid, String eventname) {
        this.eventid = eventid;
        this.eventname = eventname;
    }

    public BigDecimal getEventid() {
        return eventid;
    }

    public void setEventid(BigDecimal eventid) {
        this.eventid = eventid;
    }

    public String getEventname() {
        return eventname;
    }

    public void setEventname(String eventname) {
        this.eventname = eventname;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public String getLocationstreet() {
        return locationstreet;
    }

    public void setLocationstreet(String locationstreet) {
        this.locationstreet = locationstreet;
    }

    public BigInteger getLocationnumber() {
        return locationnumber;
    }

    public void setLocationnumber(BigInteger locationnumber) {
        this.locationnumber = locationnumber;
    }

    public BigInteger getLocationpostal() {
        return locationpostal;
    }

    public void setLocationpostal(BigInteger locationpostal) {
        this.locationpostal = locationpostal;
    }

    public String getLocationtown() {
        return locationtown;
    }

    public void setLocationtown(String locationtown) {
        this.locationtown = locationtown;
    }

    public String getLocationcountry() {
        return locationcountry;
    }

    public void setLocationcountry(String locationcountry) {
        this.locationcountry = locationcountry;
    }

    public String getPictureurl() {
        return pictureurl;
    }

    public void setPictureurl(String pictureurl) {
        this.pictureurl = pictureurl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<Persons> getPersonsCollection() {
        return personsCollection;
    }

    public void setPersonsCollection(Collection<Persons> personsCollection) {
        this.personsCollection = personsCollection;
    }

    public Collection<Labels> getLabelsCollection() {
        return labelsCollection;
    }

    public void setLabelsCollection(Collection<Labels> labelsCollection) {
        this.labelsCollection = labelsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (eventid != null ? eventid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Events)) {
            return false;
        }
        Events other = (Events) object;
        if ((this.eventid == null && other.eventid != null) || (this.eventid != null && !this.eventid.equals(other.eventid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbData.Events[ eventid=" + eventid + " ]";
    }

    public BigInteger getCreatorid() {
        return creatorid;
    }

    public void setCreatorid(BigInteger creatorid) {
        this.creatorid = creatorid;
    }

    public BigInteger getLastchanged() {
        return lastchanged;
    }

    public void setLastchanged(BigInteger lastchanged) {
        this.lastchanged = lastchanged;
    }
    
}
