/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import dbData.Persons;
import javax.ejb.Local;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;

import dbData.*;
import exceptions.UsernameTakenException;

/**
 *
 * @author Yentl
 */
@Local
public interface DatabaseBeanLocal {
    
    public Persons getPersonByID(BigDecimal id);

    public int addPerson(String username, String name, String dob, String locationStreet,
                         int locationNumber, int locationPostal, String locationTown,
                         String locationCountry, String password) throws UsernameTakenException;
    
    public int updatePerson(int personid, String username, String name, String dob,
        String locationStreet, int locationNumber, int locationPostal,
        String locationTown, String locationCountry, String pictureURL, String email);
    
    public Collection<Persons> getFollowing(Persons p);
    
    
    public Events getEventByID(BigDecimal id);
}
