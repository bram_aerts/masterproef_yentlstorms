package beans;


import javax.ejb.Stateless;
import javax.persistence.*;
import javax.persistence.Query.*;
import javax.ejb.EJB;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import dbData.*;
import exceptions.*;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.annotation.Resource;
import javax.transaction.UserTransaction;

/**
 *
 * @author Yentl
 */
@Stateless
public class DatabaseBean implements DatabaseBeanLocal {

    @PersistenceContext
    private EntityManager em;
        
    public DatabaseBean() {}
    
    @Override
    public Persons getPersonByID(BigDecimal id) {
        return em.find(Persons.class, id);
    }
    
    @Override
    public int addPerson(String username, String name, String dob,
        String locationStreet, int locationNumber, int locationPostal,
        String locationTown, String locationCountry, String password)
            throws UsernameTakenException {
        // First check if some of the input fields aren't already taken so we can use our own exceptions
        // and handle them accordingly
        // To be checked: username
        List<Persons> userResults = 
                em.createNamedQuery("Persons.findByUsername", Persons.class)
                    .setParameter("username", username)
                    .getResultList();
        if (!userResults.isEmpty()) {
            throw new UsernameTakenException("Tried making a new account with existing username: "+username);
        }
                
        // Get current highest personid and increment by 1 --> new person id
        int personID;
        List<BigDecimal> idResults =
            em.createNamedQuery("Persons.findLastPersonid", BigDecimal.class).getResultList();
        personID = (idResults.isEmpty()) ? 1 : idResults.get(0).intValue() + 1;
        
        // Set the required fields to add a new Persons entry to the database
        Persons person = new Persons();
        person.setPersonid(new BigDecimal(personID));
        person.setUsername(username);
        person.setPersonname(name);
        // Date of birth string should be in DD/MM/YYYY format
        try {
            person.setDob(new SimpleDateFormat("DD/MM/YYYY").parse(dob));
        } catch (ParseException pe) {
            person.setDob(null);
        }
        person.setLocationstreet(locationStreet);
        person.setLocationnumber(BigInteger.valueOf(locationNumber));
        person.setLocationpostal(BigInteger.valueOf(locationPostal));
        person.setLocationtown(locationTown);
        person.setLocationcountry(locationCountry);
        // TODO: encrypt password
        person.setPassword(password);
        
        em.persist(person);
        return personID;
    }
    
    @Override
    public int updatePerson(int personid, String username, String name, String dob,
        String locationStreet, int locationNumber, int locationPostal,
        String locationTown, String locationCountry, String pictureURL, String email) {
        
        System.out.println("updateperson");
        System.out.println(personid + "--" + username + "--" + name + "--" + dob);
        System.out.println(locationStreet + "--" + locationNumber + "--" + locationPostal);
        System.out.println(locationTown + "--" + locationCountry + "--" + pictureURL + "--" + email);
        
        Persons person = this.getPersonByID(new BigDecimal(personid));
        if ( person == null ) {
            System.out.println("Can't update person, doesn't exist yet!");
            return -1;
        }
        person.setUsername(username);
        person.setPersonname(name);
        // Date of birth string should be in DD/MM/YYYY format
        
        try {
            person.setDob(new SimpleDateFormat("dd/MM/yyyy").parse(dob));
        } catch (ParseException pe) {
            person.setDob(null);
        }
        person.setLocationstreet(locationStreet);
        person.setLocationnumber(BigInteger.valueOf(locationNumber));
        person.setLocationpostal(BigInteger.valueOf(locationPostal));
        person.setLocationtown(locationTown);
        person.setLocationcountry(locationCountry);
        person.setEmail(email);
        person.setPictureurl(pictureURL);
        em.persist(person);
        
        return personid;
    }
    
    @Override
    public Collection<Persons> getFollowing(Persons p) {
        //Persons p = this.getPersonByID(id);
        return p.getPersonsCollection();
    }



    @Override
    public Events getEventByID(BigDecimal id) {
        return em.find(Events.class, id);
    }}
