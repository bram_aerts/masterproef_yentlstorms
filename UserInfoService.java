package com.yentlstorms.virdirapptor.service;


import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.yentlstorms.virdirapptor.Constants;
import com.yentlstorms.virdirapptor.MultipartUtility;
import com.yentlstorms.virdirapptor.Utility;
import com.yentlstorms.virdirapptor.data.DataContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Vector;


public class UserInfoService extends IntentService {
    private final String LOG_TAG = UserInfoService.class.getSimpleName();

    private static final String ACTION_GET_USER_INFO =
            "com.yentlstorms.virdirapptor.service.userinfoservice.action.getuserinfo";
    private static final String ACTION_UPDATE_USER_INFO =
            "com.yentlstorms.virdirapptor.service.userinfoservice.action.updateuserinfo";

    // Local network server: changed on different development machines.
    private String server_ip = "192.168.0.121";
    //private String server_ip = "10.68.251.234";
    private String server_port = "8080";

    public static final String PERSONID_QUERY_EXTRA = "pqe";
    public static final String USERDATA_QUERY_EXTRA = "uqe";

    public UserInfoService() {
        super("Persons");
    }

    public static void startActionGetUserInfo(Context context, int personid) {
        Intent intent = new Intent(context, UserInfoService.class);
        intent.setAction(ACTION_GET_USER_INFO);
        intent.putExtra(PERSONID_QUERY_EXTRA, String.valueOf(personid));
        context.startService(intent);
    }

    public static void startActionUpdateUserInfo(Context context, Bundle userData) {
        Intent intent = new Intent(context, UserInfoService.class);
        intent.setAction(ACTION_UPDATE_USER_INFO);
        intent.putExtra(USERDATA_QUERY_EXTRA, userData);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GET_USER_INFO.equals(action)) {
                handleActionGetUserInfo(intent.getStringExtra(PERSONID_QUERY_EXTRA));
            } else if (ACTION_UPDATE_USER_INFO.equals(action)) {
                handleActionUpdateUserInfo(intent);
            }
        }
        return;
    }

    private void handleActionGetUserInfo(String personid) {
        Log.v(LOG_TAG, "Started handleActionGetUserInfo with personid = " + personid);

        // These two need to be declared outside the try/catch
        // so that they can be closed in the finally block.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        // Will contain the raw JSON response as a string.
        String responseJSONString;

        try {
            // Construct the URL for the Server request
            final String PERSONS_BASE_URL =
                    Constants.SERVER_BASE_URL + "Person?";
            final String ACTION_PARAM = "action";
            final String ACTION_VALUE = "getUserInfo";
            final String PERSONID_PARAM = "personid";

            Uri builtUri = Uri.parse(PERSONS_BASE_URL).buildUpon()
                    .appendQueryParameter(ACTION_PARAM, ACTION_VALUE)
                    .appendQueryParameter(PERSONID_PARAM, personid)
                    .build();

            URL url = new URL(builtUri.toString());

            // Create the request to OpenWeatherMap, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return;
            }
            responseJSONString = buffer.toString();
            Log.v(LOG_TAG, "JSON string:" + responseJSONString);
            getDataFromJSON(responseJSONString, Integer.parseInt(personid));
        } catch (ConnectException e) {
            Log.e(LOG_TAG, "Error ", e);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attempting
            // to parse it.
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(LOG_TAG, "Error closing stream", e);
                }
            }
        }
        return;
    }

    private void getDataFromJSON(String resultJSONString,
                                 int personidArg)
            throws JSONException {

        // Now we have a String representing the complete forecast in JSON Format.
        // Fortunately parsing is easy:  constructor takes the JSON string and converts it
        // into an Object hierarchy for us.

        // These are the names of the JSON objects that need to be extracted.
        final String KEY_PERSONID = "personid";
        final String KEY_USERNAME = "username";
        final String KEY_PERSONNAME = "personname";
        final String KEY_DOB = "dob";
        final String KEY_LOCATIONSTREET = "locationstreet";
        final String KEY_LOCATIONNUMBER = "locationnumber";
        final String KEY_LOCATIONPOSTAL = "locationpostal";
        final String KEY_LOCATIONTOWN = "locationtown";
        final String KEY_LOCATIONCOUNTRY = "locationcountry";
        final String KEY_PICTUREURL = "pictureurl";
        final String KEY_EMAIL = "email";

        final String KEY_FOLLOWS_LIST = "follows";

        try {
            JSONObject personsJSON = new JSONObject(resultJSONString);
            JSONArray followsArray = personsJSON.getJSONArray(KEY_FOLLOWS_LIST);

            // TODO: for now do no check, later on, can check if this personid == personidArg and handle error
            int personid = personsJSON.getInt(Constants.JSON_KEY_PERSONID);
            String username = personsJSON.getString(Constants.JSON_KEY_USERNAME);
            String personname = personsJSON.getString(Constants.JSON_KEY_PERSONNAME);
            String dob = personsJSON.getString(Constants.JSON_KEY_DOB);
            String locationstreet = personsJSON.getString(Constants.JSON_KEY_LOCATIONSTREET);
            int locationnumber = personsJSON.getInt(Constants.JSON_KEY_LOCATIONNUMBER);
            int locationpostal = personsJSON.getInt(Constants.JSON_KEY_LOCATIONPOSTAL);
            String locationtown = personsJSON.getString(Constants.JSON_KEY_LOCATIONTOWN);
            String locationcountry = personsJSON.getString(Constants.JSON_KEY_LOCATIONCOUNTRY);
            String pictureurl = personsJSON.getString(Constants.JSON_KEY_PICTUREURL);
            String email = personsJSON.getString(Constants.JSON_KEY_EMAIL);

            // add the person through the content provider (DataProvider)
            ContentValues personValues;
            personValues = new ContentValues();
            // As this database insert is based upon the primary key (personid), our data provider
            // uses the insertWithOnConflict method with REPLACE on conflict.
            // This basically means that if the person doesn't exist yet, it'll insert it,
            // if it does exist, it'll replace it with the new values.

            // set the ContentValues' data.
            personValues.put(DataContract.PersonsEntry._ID, personid);
            personValues.put(DataContract.PersonsEntry.COLUMN_USERNAME, username);
            personValues.put(DataContract.PersonsEntry.COLUMN_PERSONNAME, personname);
            personValues.put(DataContract.PersonsEntry.COLUMN_DOB, dob);
            personValues.put(DataContract.PersonsEntry.COLUMN_LOCATION_STREET, locationstreet);
            personValues.put(DataContract.PersonsEntry.COLUMN_LOCATION_NUMBER, locationnumber);
            personValues.put(DataContract.PersonsEntry.COLUMN_LOCATION_POSTAL, locationpostal);
            personValues.put(DataContract.PersonsEntry.COLUMN_LOCATION_TOWN, locationtown);
            personValues.put(DataContract.PersonsEntry.COLUMN_LOCATION_COUNTRY, locationcountry);
            //personValues.put(DataContract.PersonsEntry.COLUMN_PICTURE_URL, pictureurl);
            personValues.put(DataContract.PersonsEntry.COLUMN_EMAIL, email);


            // Download the picture from the server (specified in pictureurl) and save it client-side.
            // TODO: If it already exists (unique names: combo ID + time of creation), no need to download
            final String PROFILE_PICTURES_BASE_URL =
                    Constants.SERVER_BASE_URL + "file" + File.separator + "ProfilePictures";
            try {
                Log.v(LOG_TAG, "download image (start)");
                URL source = new URL(PROFILE_PICTURES_BASE_URL + File.separator + pictureurl);
                Log.v(LOG_TAG, "source = "+source.toString());
                String profilePicsDirPath = getFilesDir().getAbsolutePath() + File.separator + Constants.DIR_PROFILE_PICTURES;
                File destination = new File(profilePicsDirPath + File.separator + pictureurl);
                Log.v(LOG_TAG, "destination = " + destination.getAbsolutePath().toString());
                Log.v(LOG_TAG, source.toString());
                if (Utility.downloadImageFromServer(source, destination)) {
                    personValues.put(DataContract.PersonsEntry.COLUMN_PICTURE_URL, destination.getAbsolutePath().toString());
                } else {
                    personValues.put(DataContract.PersonsEntry.COLUMN_PICTURE_URL, "");
                }
            } catch (MalformedURLException mfURLExc) {
                Log.v(LOG_TAG, mfURLExc.toString());
                personValues.put(DataContract.PersonsEntry.COLUMN_PICTURE_URL, "");
            }


            // and insert it into the database
            Uri insertedUri = this.getContentResolver().insert(
                    DataContract.PersonsEntry.CONTENT_URI,
                    personValues
            );

            Vector<ContentValues> followsCVVector = new Vector<ContentValues>(followsArray.length());
            int persontwoid;
            for(int i = 0; i < followsArray.length(); i++) {
                // in the follows array, we only get a subset of the
                // as we don't want the user to know the exact location,... of other users
                // Example of a single follows element:
                //{"dob":"Sun Dec 01 00:00:00 CET 1996","pictureurl":"some-picture-url",
                // "personid":3,"locationcountry":"DE","username":"client","personname":"Some Client Name"}

                JSONObject followsElement = followsArray.getJSONObject(i);

                persontwoid = followsElement.getInt(Constants.JSON_KEY_PERSONID);
                username = followsElement.getString(Constants.JSON_KEY_USERNAME);
                personname = followsElement.getString(Constants.JSON_KEY_PERSONNAME);
                dob = followsElement.getString(Constants.JSON_KEY_DOB);
                locationcountry = followsElement.getString(Constants.JSON_KEY_LOCATIONCOUNTRY);
                pictureurl = followsElement.getString(Constants.JSON_KEY_PICTUREURL);

                ContentValues followsValues = new ContentValues();

                // "" fields with values are not to be known by other people but the user himself.
                followsValues.put(DataContract.PersonsEntry._ID, persontwoid);
                followsValues.put(DataContract.PersonsEntry.COLUMN_USERNAME, username);
                followsValues.put(DataContract.PersonsEntry.COLUMN_PERSONNAME, personname);
                followsValues.put(DataContract.PersonsEntry.COLUMN_DOB, dob);
                followsValues.put(DataContract.PersonsEntry.COLUMN_LOCATION_STREET, "");
                followsValues.put(DataContract.PersonsEntry.COLUMN_LOCATION_NUMBER, "");
                followsValues.put(DataContract.PersonsEntry.COLUMN_LOCATION_POSTAL, "");
                followsValues.put(DataContract.PersonsEntry.COLUMN_LOCATION_TOWN, "");
                followsValues.put(DataContract.PersonsEntry.COLUMN_LOCATION_COUNTRY, locationcountry);
                //followsValues.put(DataContract.PersonsEntry.COLUMN_PICTURE_URL, pictureurl);
                followsValues.put(DataContract.PersonsEntry.COLUMN_EMAIL, "");

                /* downloading of the images the person follows */
                try {
                    Log.v(LOG_TAG, "download image (start)");
                    URL source = new URL(PROFILE_PICTURES_BASE_URL + File.separator + pictureurl);
                    Log.v(LOG_TAG, "source = "+source.toString());
                    String profilePicsDirPath = getFilesDir().getAbsolutePath() + File.separator + Constants.DIR_PROFILE_PICTURES;
                    File destination = new File(profilePicsDirPath + File.separator + pictureurl);
                    Log.v(LOG_TAG, "destination = " + destination.getAbsolutePath().toString());
                    Log.v(LOG_TAG, source.toString());
                    if (Utility.downloadImageFromServer(source, destination)) {
                        followsValues.put(DataContract.PersonsEntry.COLUMN_PICTURE_URL, destination.getAbsolutePath().toString());
                    } else {
                        followsValues.put(DataContract.PersonsEntry.COLUMN_PICTURE_URL, "");
                    }
                } catch (MalformedURLException mfURLExc) {
                    Log.v(LOG_TAG, mfURLExc.toString());
                    followsValues.put(DataContract.PersonsEntry.COLUMN_PICTURE_URL, "");
                }

                followsCVVector.add(followsValues);

                // make the link between the main user and the people he follows:
                // TODO: move this to a bulk insert
                ContentValues followsTableValues = new ContentValues();
                followsTableValues.put(DataContract.FollowsEntry._ID, personid);
                followsTableValues.put(DataContract.FollowsEntry.COLUMN_PERSONTWO_ID, persontwoid);
                // and insert it into the database
                this.getContentResolver().insert(
                        DataContract.FollowsEntry.CONTENT_URI,
                        followsTableValues
                );
            }

            int inserted = 0;
            // add to database
            if ( followsCVVector.size() > 0 ) {
                ContentValues[] cvArray = new ContentValues[followsCVVector.size()];
                followsCVVector.toArray(cvArray);
                this.getContentResolver().bulkInsert(DataContract.PersonsEntry.CONTENT_URI, cvArray);
            }

            Log.d(LOG_TAG, "UserInfoService Complete (GetUserInfo). " + followsCVVector.size() + " follows Inserted");
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
    }

    private void handleActionUpdateUserInfo(Intent intent) {
        Bundle userData = intent.getBundleExtra(USERDATA_QUERY_EXTRA);
        if (userData == null) {
            //TODO: handle this correctly
            return;
        }
        Log.v(LOG_TAG, "Started handleActionUpdateUserInfo for personid = " + userData.getInt(Constants.JSON_KEY_PERSONID));

        final String CHARSET = "UTF-8";
        final String PERSONS_BASE_URL = Constants.SERVER_BASE_URL + "upload";
        final String ACTION_PARAM = "action";
        final String ACTION_VALUE = "updateUserInfo";
        try {
            Bundle locationBundle = userData.getBundle(Constants.BUNDLE_KEY_LOCATION);

            MultipartUtility multipart = new MultipartUtility(PERSONS_BASE_URL, CHARSET);
            multipart.addFormField(ACTION_PARAM, ACTION_VALUE);
            multipart.addFormField(Constants.JSON_KEY_PERSONID,
                    String.valueOf(userData.getInt(Constants.BUNDLE_KEY_PERSONID)));
            multipart.addFormField(Constants.JSON_KEY_USERNAME,
                    userData.getString(Constants.BUNDLE_KEY_USERNAME));
            multipart.addFormField(Constants.JSON_KEY_PERSONNAME,
                    userData.getString(Constants.BUNDLE_KEY_PERSONNAME));
            multipart.addFormField(Constants.JSON_KEY_DOB,
                    Utility.dateBundleToDatabaseDate(userData.getBundle(Constants.BUNDLE_KEY_DOB)));
            multipart.addFormField(Constants.JSON_KEY_LOCATIONSTREET,
                    locationBundle.getString(Constants.BUNDLE_KEY_LOCATION_STREET));
            multipart.addFormField(Constants.JSON_KEY_LOCATIONNUMBER,
                    String.valueOf(locationBundle.getInt(Constants.BUNDLE_KEY_LOCATION_NUMBER)));
            multipart.addFormField(Constants.JSON_KEY_LOCATIONPOSTAL,
                    String.valueOf(locationBundle.getInt(Constants.BUNDLE_KEY_LOCATION_ZIP)));
            multipart.addFormField(Constants.JSON_KEY_LOCATIONTOWN,
                    locationBundle.getString(Constants.BUNDLE_KEY_LOCATION_TOWN));
            multipart.addFormField(Constants.JSON_KEY_LOCATIONCOUNTRY,
                    locationBundle.getString(Constants.BUNDLE_KEY_LOCATION_COUNTRY));
            multipart.addFormField(Constants.JSON_KEY_EMAIL,
                    userData.getString(Constants.BUNDLE_KEY_EMAIL));
            //.appendQueryParameter(Constants.JSON_KEY_PICTUREURL, userData.getString(Constants.BUNDLE_KEY_CROPPED_PICTURE))


        } catch (IOException ioe) {

        }


        // These two need to be declared outside the try/catch,
        // so that they can be closed in the finally block.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        try {
            // Construct the URL for the Server request
            final String PERSONS_BASE_URL =
                    Constants.SERVER_BASE_URL + "upload";
            //final String ACTION_PARAM = "action";
            //final String ACTION_VALUE = "updateUserInfo";

            Bundle locationBundle = userData.getBundle(Constants.BUNDLE_KEY_LOCATION);
            Uri builtUri = Uri.parse(PERSONS_BASE_URL).buildUpon()
                    //.appendQueryParameter(ACTION_PARAM, ACTION_VALUE)
                    .appendQueryParameter(Constants.JSON_KEY_PERSONID, String.valueOf(userData.getInt(Constants.BUNDLE_KEY_PERSONID)))
                    .appendQueryParameter(Constants.JSON_KEY_USERNAME, userData.getString(Constants.BUNDLE_KEY_USERNAME))
                    .appendQueryParameter(Constants.JSON_KEY_PERSONNAME, userData.getString(Constants.BUNDLE_KEY_PERSONNAME))
                    .appendQueryParameter(Constants.JSON_KEY_DOB,
                            Utility.dateBundleToDatabaseDate(userData.getBundle(Constants.BUNDLE_KEY_DOB)))
                    .appendQueryParameter(Constants.JSON_KEY_LOCATIONSTREET, locationBundle.getString(Constants.BUNDLE_KEY_LOCATION_STREET))
                    .appendQueryParameter(Constants.JSON_KEY_LOCATIONNUMBER, String.valueOf(locationBundle.getInt(Constants.BUNDLE_KEY_LOCATION_NUMBER)))
                    .appendQueryParameter(Constants.JSON_KEY_LOCATIONPOSTAL, String.valueOf(locationBundle.getInt(Constants.BUNDLE_KEY_LOCATION_ZIP)))
                    .appendQueryParameter(Constants.JSON_KEY_LOCATIONTOWN, locationBundle.getString(Constants.BUNDLE_KEY_LOCATION_TOWN))
                    .appendQueryParameter(Constants.JSON_KEY_LOCATIONCOUNTRY, locationBundle.getString(Constants.BUNDLE_KEY_LOCATION_COUNTRY))
                            //.appendQueryParameter(Constants.JSON_KEY_PICTUREURL, userData.getString(Constants.BUNDLE_KEY_CROPPED_PICTURE))
                    .appendQueryParameter(Constants.JSON_KEY_EMAIL, userData.getString(Constants.BUNDLE_KEY_EMAIL))
                    .build();

            URL url = new URL(builtUri.toString());
            Log.v(LOG_TAG, "Request url used = " + url);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            Log.v(LOG_TAG, "imageFile: " + userData.getString(Constants.BUNDLE_KEY_CROPPED_PICTURE));
            try {
                URI uri = new URI(userData.getString(Constants.BUNDLE_KEY_CROPPED_PICTURE));
                File imageFile = new File(uri);
                Log.v(LOG_TAG, "imageFile is file?: " + imageFile.isFile());
                Utility.uploadImageToServer(imageFile);
            } catch (URISyntaxException usExc) {

            }
            /*
            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));

            // TODO: Handle a result message
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.contains("succesful")) {
                    return;
                }
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return;
            }*/
        } catch (ConnectException e) {
            Log.e(LOG_TAG, "Error ", e);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attempting
            // to parse it.
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(LOG_TAG, "Error closing stream", e);
                }
            }
        }
        return;
    }

}