alter session set nls_date_format = 'DD/MM/YYYY HH24:MI:SS';

INSERT INTO "VIRDIRAPPTOR".PERSONS VALUES (
    '1',
    'owner',
    'Yentl Storms',
    '20/02/1995',
    'Bergstraat',
    '9',
    '2223',
    'Schriek',
    'BE',
    '1.jpg',
    'yentl.storms@student.kuleuven.be',
    'somepassword',
    0
);

INSERT INTO "VIRDIRAPPTOR".PERSONS VALUES (
    '2',
    'user',
    'Some User Name',
    '01/12/2000',
    'Some street name',
    '101',
    '9999',
    'Some placename',
    'DE',
    '2.jpg',
    'some-email@adres.com',
    'some-encrypted-pass',
    0
);

INSERT INTO "VIRDIRAPPTOR".PERSONS VALUES (
    '3',
    'client',
    'Some Client Name',
    '01/12/1996',
    'Some street name',
    '101',
    '9999',
    'Some placename',
    'DE',
    '3.jpg',
    'some-email@adres.com',
    'some-encrypted-pass',
    0
);

INSERT INTO "VIRDIRAPPTOR".PERSONS VALUES (
    '4',
    'client2',
    'Some Client Name2',
    '01/12/1997',
    'Some street name2',
    '101',
    '9999',
    'Some placename2',
    'DE',
    '4.jpg',
    'some-email2@adres.com',
    'some-encrypted-pass2',
    0
);

INSERT INTO "VIRDIRAPPTOR".PERSONS VALUES (
    '5',
    'client3',
    'Some Client Name3',
    '01/12/1998',
    'Some street name3',
    '101',
    '9999',
    'Some placename3',
    'DE',
    '5.jpg',
    'some-email3@adres.com',
    'some-encrypted-pass3',
    0
);

INSERT INTO "VIRDIRAPPTOR".PERSONS VALUES (
    '6',
    'client4',
    'Some Client Name4',
    '01/12/1999',
    'Some street name4',
    '101',
    '9999',
    'Some placename4',
    'DE',
    '6.jpg',
    'some-email4@adres.com',
    'some-encrypted-pass4',
    0
);

INSERT INTO "VIRDIRAPPTOR".PERSONS VALUES (
    '7',
    'client5',
    'Some Client Name4',
    '01/12/1999',
    'Some street name4',
    '101',
    '9999',
    'Some placename4',
    'DE',
    '7.jpg',
    'some-email4@adres.com',
    'some-encrypted-pass4',
    0
);

INSERT INTO "VIRDIRAPPTOR".PERSONS VALUES (
    '8',
    'client6',
    'Some Client Name4',
    '01/12/1999',
    'Some street name4',
    '101',
    '9999',
    'Some placename4',
    'DE',
    '8.jpg',
    'some-email4@adres.com',
    'some-encrypted-pass4',
    0
);

INSERT INTO "VIRDIRAPPTOR".PERSONS VALUES (
    '9',
    'client7',
    'Some Client Name4',
    '01/12/1999',
    'Some street name4',
    '101',
    '9999',
    'Some placename4',
    'DE',
    '1.jpg',
    'some-email4@adres.com',
    'some-encrypted-pass4',
    0
);

INSERT INTO "VIRDIRAPPTOR".PERSONS VALUES (
    '10',
    'client8',
    'Some Client Name4',
    '01/12/1999',
    'Some street name4',
    '101',
    '9999',
    'Some placename4',
    'DE',
    '2.jpg',
    'some-email4@adres.com',
    'some-encrypted-pass4',
    0
);

INSERT INTO "VIRDIRAPPTOR".PERSONS VALUES (
    '11',
    'client9',
    'Some Client Name4',
    '01/12/1999',
    'Some street name4',
    '101',
    '9999',
    'Some placename4',
    'DE',
    '3.jpg',
    'some-email4@adres.com',
    'some-encrypted-pass4',
    0
);

INSERT INTO "VIRDIRAPPTOR".PERSONS VALUES (
    '12',
    'client10',
    'Some Client Name4',
    '01/12/1999',
    'Some street name4',
    '101',
    '9999',
    'Some placename4',
    'DE',
    '4.jpg',
    'some-email4@adres.com',
    'some-encrypted-pass4',
    0
);

INSERT INTO "VIRDIRAPPTOR".EVENTS VALUES (
    '1',
    'Defqon. 1',
    '24/06/2016 12:00:00',
    '26/06/2016 23:59:59',
    'Spijkweg',
    '30',
    '8256',
    'RJ Biddinghuizen',
    'NL',
    'some-picture-url',
    'A description about the event for the users to read.',
    1,
    0
);

INSERT INTO "VIRDIRAPPTOR".EVENTS VALUES (
    '2',
    'Some event name',
    '20/02/2016 05:00:00',
    '26/05/2016 23:59:59',
    'Hoogstraat',
    '30',
    '2250',
    'Schriek',
    'BE',
    'some-picture-url2',
    'A description about the event for the users to read.',
    1,
    0
);

INSERT INTO "VIRDIRAPPTOR".PERSONS_EVENTS VALUES (
    '1', '1'
);

INSERT INTO "VIRDIRAPPTOR".PERSONS_EVENTS VALUES (
    '2', '1'
);

INSERT INTO "VIRDIRAPPTOR".LABELS VALUES (
    '1',
    'Music'
);

INSERT INTO "VIRDIRAPPTOR".LABELS VALUES (
    '2',
    'Sport'
);

INSERT INTO "VIRDIRAPPTOR".EVENTS_LABELS VALUES (
    '1', 
    '1'
);

INSERT INTO "VIRDIRAPPTOR".FOLLOW VALUES (
    '1',
    '1'
);

INSERT INTO "VIRDIRAPPTOR".FOLLOWS VALUES (
    '1',
    '2'
);

INSERT INTO "VIRDIRAPPTOR".FOLLOWS VALUES (
    '1',
    '3'
);

INSERT INTO "VIRDIRAPPTOR".FOLLOWS VALUES (
    '1',
    '4'
);

INSERT INTO "VIRDIRAPPTOR".FOLLOWS VALUES (
    '1',
    '5'
);

INSERT INTO "VIRDIRAPPTOR".FOLLOWS VALUES (
    '1',
    '6'
);

INSERT INTO "VIRDIRAPPTOR".FOLLOWS VALUES (
    '1',
    '7'
);

INSERT INTO "VIRDIRAPPTOR".FOLLOWS VALUES (
    '1',
    '8'
);

INSERT INTO "VIRDIRAPPTOR".FOLLOWS VALUES (
    '1',
    '9'
);

INSERT INTO "VIRDIRAPPTOR".FOLLOWS VALUES (
    '1',
    '10'
);

INSERT INTO "VIRDIRAPPTOR".FOLLOWS VALUES (
    '1',
    '11'
);

INSERT INTO "VIRDIRAPPTOR".FOLLOWS VALUES (
    '1',
    '12'
);

INSERT INTO "VIRDIRAPPTOR".FOLLOWS VALUES (
    '2',
    '1'
);