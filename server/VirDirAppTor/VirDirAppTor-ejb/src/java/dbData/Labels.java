/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbData;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Yentl
 */
@Entity
@Table(name = "LABELS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Labels.findAll", query = "SELECT l FROM Labels l"),
    @NamedQuery(name = "Labels.findByLabelid", query = "SELECT l FROM Labels l WHERE l.labelid = :labelid"),
    @NamedQuery(name = "Labels.findByLabelname", query = "SELECT l FROM Labels l WHERE l.labelname = :labelname")})
public class Labels implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "LABELID")
    private BigDecimal labelid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "LABELNAME")
    private String labelname;
    @ManyToMany(mappedBy = "labelsCollection")
    private Collection<Events> eventsCollection;

    public Labels() {
    }

    public Labels(BigDecimal labelid) {
        this.labelid = labelid;
    }

    public Labels(BigDecimal labelid, String labelname) {
        this.labelid = labelid;
        this.labelname = labelname;
    }

    public BigDecimal getLabelid() {
        return labelid;
    }

    public void setLabelid(BigDecimal labelid) {
        this.labelid = labelid;
    }

    public String getLabelname() {
        return labelname;
    }

    public void setLabelname(String labelname) {
        this.labelname = labelname;
    }

    @XmlTransient
    public Collection<Events> getEventsCollection() {
        return eventsCollection;
    }

    public void setEventsCollection(Collection<Events> eventsCollection) {
        this.eventsCollection = eventsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (labelid != null ? labelid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Labels)) {
            return false;
        }
        Labels other = (Labels) object;
        if ((this.labelid == null && other.labelid != null) || (this.labelid != null && !this.labelid.equals(other.labelid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbData.Labels[ labelid=" + labelid + " ]";
    }
    
}
