/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package json;

import dbData.Persons;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Yentl
 */
public class PersonsJSON {
    
    public static JSONObject makeCreatorJSON(Persons person) {
        JSONObject obj = new JSONObject();
        Calendar cal = Calendar.getInstance();
        cal.setTime(person.getDob());
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        obj.put("personid", person.getPersonid());
        obj.put("username", person.getUsername());
        obj.put("personname", person.getPersonname());
        obj.put("dob", sdf.format(person.getDob()));
        obj.put("locationcountry", person.getLocationcountry());
        obj.put("pictureurl", person.getPictureurl());
        obj.put("lastchanged", person.getLastchanged());
        
        return obj;
    }
    
    public static JSONObject makeJSON(Persons person, boolean isLoggedInUser) {
        // TODO: Check if requested person == logged-in person 
        //  --> less or more information given depending on this.
        try {
            JSONObject obj = new JSONObject();
            Calendar cal = Calendar.getInstance();
            cal.setTime(person.getDob());
            //System.out.println(cal.get(Calendar.MONTH));
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            obj.put("personid", person.getPersonid());
            obj.put("username", person.getUsername());
            obj.put("personname", person.getPersonname());
            obj.put("dob", sdf.format(person.getDob()));
            obj.put("locationcountry", person.getLocationcountry());
            obj.put("pictureurl", person.getPictureurl());
            obj.put("lastchanged", person.getLastchanged());
            if (isLoggedInUser) {
                obj.put("locationstreet", person.getLocationstreet());
                obj.put("locationnumber", person.getLocationnumber());
                obj.put("locationpostal", person.getLocationpostal());
                obj.put("locationtown", person.getLocationtown());
                obj.put("email", person.getEmail());
            }
            JSONArray followsArray = new JSONArray();
            for (Persons follow : person.getPersonsCollection()) {
                JSONObject followObj = new JSONObject();
                followObj.put("personid", follow.getPersonid());
                followObj.put("username", follow.getUsername());
                followObj.put("personname", follow.getPersonname());
                followObj.put("dob", sdf.format(follow.getDob()));
                followObj.put("locationcountry", follow.getLocationcountry());
                followObj.put("pictureurl", follow.getPictureurl());
                followObj.put("lastchanged", follow.getLastchanged());
                followsArray.add(followObj);
            }
            obj.put("follows", followsArray);
            // TODO: add basic events info
            
            return obj;
        } catch (Exception e) {
            e.printStackTrace();
            JSONObject obj = new JSONObject();
            obj.put("result", "error");
            return obj;
        }
    }
    
    
}
