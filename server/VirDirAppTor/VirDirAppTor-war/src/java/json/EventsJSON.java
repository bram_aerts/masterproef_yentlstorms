/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package json;

import dbData.Persons;
import dbData.Events;
import dbData.Labels;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Yentl
 */
public class EventsJSON {
    
    public static JSONObject makeJSON(Events event) {
        try {
            JSONObject result = new JSONObject();
            
            JSONObject eventJSON = new JSONObject();
            Calendar cal = Calendar.getInstance();
            //cal.setTime(p.getDob());
            //System.out.println(cal.get(Calendar.MONTH));
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
            //obj.put("dob", sdf.format(p.getDob()));
            eventJSON.put("eventid", event.getEventid());
            eventJSON.put("eventname", event.getEventname());
            eventJSON.put("startdate", sdf.format(event.getStartdate()));
            eventJSON.put("enddate", sdf.format(event.getEnddate()));
            eventJSON.put("locationstreet", event.getLocationstreet());
            eventJSON.put("locationnumber", event.getLocationnumber());
            eventJSON.put("locationpostal", event.getLocationpostal());
            eventJSON.put("locationtown", event.getLocationtown());
            eventJSON.put("locationcountry", event.getLocationcountry());
            eventJSON.put("pictureurl", event.getPictureurl());
            eventJSON.put("description", event.getDescription());
            eventJSON.put("lastchanged", event.getLastchanged());
            
            eventJSON.put("creator", PersonsJSON.makeCreatorJSON(event.getCreatorid()));
            
            JSONArray labelsArray = new JSONArray();
            for (Labels label : event.getLabelsCollection()) {
                JSONObject labelObject = new JSONObject();
                labelObject.put("labelid", label.getLabelid());
                labelObject.put("labelname", label.getLabelname());
                labelsArray.add(labelObject);
            }
            eventJSON.put("labels", labelsArray);
            
            result.put("event", eventJSON);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            JSONObject obj = new JSONObject();
            obj.put("result", "error");
            return obj;
        }
    }
    
    public static JSONObject makeJSON(Persons person) {
        try {
            JSONObject result = new JSONObject();
            JSONArray eventArray = new JSONArray();
            for (Events event : person.getEventsCollection()) {
                eventArray.add(EventsJSON.makeJSON(event));
            }
            result.put("events", eventArray);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            JSONObject result = new JSONObject();
            result.put("result", "error");
            return result;
        }
    }
    
}
