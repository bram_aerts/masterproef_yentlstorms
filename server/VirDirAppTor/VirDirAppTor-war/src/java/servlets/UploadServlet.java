package servlets;

// Import required java libraries
// http://www.tutorialspoint.com/servlets/servlets-file-uploading.htm
import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.*;
 
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
 
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.output.*;

@MultipartConfig
public class UploadServlet extends HttpServlet {
    private boolean isMultipart;
    private String filePath;
    private final int maxFileSize = 1024 * 1024;
    private final int maxMemSize = 1024 * 1024;
    private File file ;

    public void init( ){
        // Get the file location where it would be stored.
        //filePath = getServletContext().getInitParameter("file-upload"); 
        filePath= "C:\\Users\\Yentl\\Desktop\\master\\master\\server\\VirDirAppTor\\files\\";
    }
   
    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, java.io.IOException {
        // Check that we have a file upload request
        System.out.println("UploadServlet started (POST)");
        isMultipart = ServletFileUpload.isMultipartContent(request);
        response.setContentType("text/html");
        java.io.PrintWriter out = response.getWriter();
        if( !isMultipart ){
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet upload</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<p>No file uploaded</p>"); 
            out.println("</body>");
            out.println("</html>");
            return;
        }
        
        //System.out.println(request.getParameter("username"));
        Part filePart = request.getPart("pictureurl");
        String fileName = filePart.getSubmittedFileName();
        System.out.println(fileName);
        System.out.println(filePart.getSize());
        File uploadDir = new File(filePath + File.separator + "ProfilePictures");
        File uploadedFile = new File(uploadDir, fileName);
        System.out.println("uploadedFile path = " + uploadedFile.toPath());
        try (InputStream input = filePart.getInputStream()) {
            System.out.println(input);
            Files.copy(input, uploadedFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
        
        // Dont use filePart.write(String filename) --> writes relative to configured TEMP directory!
        
        /*
        DiskFileItemFactory factory = new DiskFileItemFactory();
        // maximum size that will be stored in memory
        factory.setSizeThreshold(maxMemSize);
        // Location to save data that is larger than maxMemSize.
        factory.setRepository(new File("c:\\temp"));

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);
        // maximum file size to be uploaded.
        upload.setSizeMax( maxFileSize );

        try{ 
            // Parse the request to get file items.
            List fileItems = upload.parseRequest(request);
            
            // Process the uploaded file items
            Iterator i = fileItems.iterator();

            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet upload</title>");  
            out.println("</head>");
            out.println("<body>");
            while ( i.hasNext () ) {
                FileItem fi = (FileItem)i.next();
                if ( fi.isFormField() ) {
                    System.out.println(fi.getFieldName() + " --- " + fi.getString());
                }
                if ( !fi.isFormField () ) {
                    // Get the uploaded file parameters
                    String fieldName = fi.getFieldName();
                    String fileName = fi.getName();
                    String contentType = fi.getContentType();
                    boolean isInMemory = fi.isInMemory();
                    long sizeInBytes = fi.getSize();
                    // Write the file
                    if( fileName.lastIndexOf("\\") >= 0 ) {
                        file = new File( filePath +
                                fileName.substring( fileName.lastIndexOf("\\")));
                    } else {
                        file = new File( filePath +
                                fileName.substring(fileName.lastIndexOf("\\")+1));
                    }
                    fi.write( file );
                    out.println("Uploaded Filename: " + fileName + "<br>");
                }
            }
            out.println("</body>");
            out.println("</html>");
        } catch (Exception ex) {
            System.out.println(ex);
        }*/
    }
    
    public void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, java.io.IOException {
        
        throw new ServletException("GET method used with " +
                getClass( ).getName( )+": POST method required.");
    } 
}