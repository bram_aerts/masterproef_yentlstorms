package servlets;

import beans.DatabaseBeanLocal;
import dbData.Events;
import dbData.Persons;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import json.EventsJSON;
import json.PersonsJSON;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

/**
 *
 * @author Yentl
 */
@WebServlet(name = "EventHandler", urlPatterns = {"/Event"})
public class EventHandler extends HttpServlet {

    @EJB
    private DatabaseBeanLocal dbBean;
    
    private String savePath;
    
    public void init( ){
        //this.savePath = getServletContext().getInitParameter("profPicsPath");
        this.savePath = "C:\\Users\\Yentl\\Desktop\\master\\master\\server\\VirDirAppTor\\files\\EventPictures\\";
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        System.out.println("EventHandler - processRequest");
        try (PrintWriter out = response.getWriter()) {
            String action = request.getParameter("action");
            response.setContentType("application/json;charset=UTF-8");
            switch (action) {
                case "getEventInfo" :
                    if ( request.getParameterMap().containsKey("eventid") ) {
                        Events event = dbBean.getEventByID(new BigDecimal(request.getParameter("eventid")));
                        out.println(EventsJSON.makeJSON(event).toJSONString());
                        break;
                    } else if ( request.getParameterMap().containsKey("personid") ) {
                        JSONObject obj = new JSONObject();
                        Persons person = dbBean.getPersonByID(new BigDecimal(request.getParameter("personid")));
                        out.println(EventsJSON.makeJSON(person).toJSONString());
                        break;                        
                    }
                case "updateEventInfo" :
                    
                    break;
                case "createNewEvent" :
                    break;
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
