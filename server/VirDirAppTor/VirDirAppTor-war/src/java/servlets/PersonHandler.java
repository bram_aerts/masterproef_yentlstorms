package servlets;

import beans.*;
import dbData.Persons;
import javax.ejb.EJB;

import dbData.*;
import java.io.File;

import json.PersonsJSON;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import org.json.simple.*;


/**
 *
 * @author Yentl
 */
@WebServlet(name = "PersonsHandler", urlPatterns = "/Person")
@MultipartConfig(location="C:/Users/Yentl/AppData/Local/Temp", maxFileSize=20848820)

// location, fileSizeThreshold, MaxFileSize, maxRequestSize
public class PersonHandler extends HttpServlet {
    private String savePath;
    
    public void init( ){
        //this.savePath = getServletContext().getInitParameter("profPicsPath");
        this.savePath = "C:\\Users\\Yentl\\Desktop\\master\\master\\server\\VirDirAppTor\\files\\ProfilePictures\\";
    }
    
    @EJB
    private DatabaseBeanLocal dbBean;
   
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet re sponse
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO: add query parameter to decide what exactly is requested. (action)
        //response.setContentType("text/html;charset=UTF-8");
        System.out.println("PersonHandler - processRequest");
        try (PrintWriter out = response.getWriter()) {
            String action = request.getParameter("action");
            switch (action) {
                case "createNew" :
                    break;
                case "updateUserInfo" :
                    updateUserInfo(request);
                    break;
                case "getUserInfo" :
                    response.setContentType("application/json;charset=UTF-8");
                    Persons person = dbBean.getPersonByID(new BigDecimal(request.getParameter("personid")));
                    out.println(PersonsJSON.makeJSON(person, true).toJSONString());
                    break;
            }
        } catch (Exception e) {
            ;
        }
    }
    
    /**
      * Updating user info includes uploading the profile picture from client to server.
      * This requires a POST request, with multipart/form-data.
      * We use UploadServlet to handle requests including uploads.
      *
     */
    protected void updateUserInfo(HttpServletRequest request) {
        System.out.println("UpdateUserInfo started");
                
        if ( !ServletFileUpload.isMultipartContent(request) ) {
            System.out.println("updateUserInfo: NOT MULTIPART!");
            return;
        }

        // First, download the image
        String fileName = "";
        try {
            Part filePart = request.getPart("pictureurl");
            fileName = filePart.getSubmittedFileName();
            File uploadDir = new File(this.savePath);
            File imageDestination = new File(uploadDir, fileName);
            System.out.println("submitted filename = "+fileName);
            try (InputStream input = filePart.getInputStream()) {
                // REPLACE_EXISTING used, however image names use an unique timebased name
                // So collisions should never occur.
                System.out.println(imageDestination.toPath());
                Files.copy(input, imageDestination.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (ServletException servException) {
            System.out.println(servException);
        } catch (IOException ioe) {
            System.out.println(ioe);
        } catch (Exception exc) {
            System.out.println(exc);
        }
        
        // Next, get the rest of the user information and update DB.        
        System.out.println(request.getParameter("personid"));
        if ( dbBean.updatePerson(
                Integer.parseInt(request.getParameter("personid")),
                request.getParameter("username"),
                request.getParameter("personname"),
                request.getParameter("dob"),
                request.getParameter("locationstreet"),
                Integer.parseInt(request.getParameter("locationnumber")),
                Integer.parseInt(request.getParameter("locationpostal")),
                request.getParameter("locationtown"),
                request.getParameter("locationcountry"),
                fileName,
                request.getParameter("email")
        ) < 0 ) {
            System.out.println("Could not updatePerson: "+request.getParameter("personid"));   
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
