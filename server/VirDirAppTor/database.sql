DROP TABLE "VIRDIRAPPTOR".FOLLOWS;
DROP TABLE "VIRDIRAPPTOR".EVENTS_LABELS;
DROP TABLE "VIRDIRAPPTOR".LABELS;
DROP TABLE "VIRDIRAPPTOR".PERSONS_EVENTS;
DROP TABLE "VIRDIRAPPTOR".EVENTS;
DROP TABLE "VIRDIRAPPTOR".PERSONS;

CREATE TABLE "VIRDIRAPPTOR".PERSONS (
    personid        INT             PRIMARY KEY,
    username        VARCHAR2(50)    NOT NULL UNIQUE,
    personname      VARCHAR2(50)    NOT NULL,
    dob             DATE,
    locationstreet  VARCHAR2(50),
    locationnumber  INT,
    locationpostal  INT,
    locationtown    VARCHAR2(50),
    locationcountry VARCHAR2(10),
    pictureurl      VARCHAR2(100),
    email           VARCHAR2(100),
    password        VARCHAR2(100),
    lastchanged     VARCHAR2(14)
);

CREATE TABLE "VIRDIRAPPTOR".EVENTS (
    eventid         INT             PRIMARY KEY,
    eventname       VARCHAR2(50)    NOT NULL,
    startdate       DATE,
    enddate         DATE,
    locationstreet  VARCHAR2(50),
    locationnumber  INT,
    locationpostal  INT,
    locationtown    VARCHAR2(50),
    locationcountry VARCHAR2(10),
    pictureurl      VARCHAR2(100),
    description     VARCHAR2(200),
    creatorid       INT,
    lastchanged     INT,
    CONSTRAINT FK_EVENTS_CREATOR_ID FOREIGN KEY (creatorid) REFERENCES PERSONS(personid)
);

CREATE TABLE "VIRDIRAPPTOR".PERSONS_EVENTS (
    personid        INT,
    eventid         INT,
    CONSTRAINT PERSONS_EVENTS_PK PRIMARY KEY (personid, eventid),
    CONSTRAINT FK_PERSONS_EVENTS_PERSON FOREIGN KEY (personid) REFERENCES PERSONS(personid),
    CONSTRAINT FK_PERSONS_EVENTS_EVENT FOREIGN KEY (eventid) REFERENCES EVENTS(eventid)
);

CREATE TABLE "VIRDIRAPPTOR".LABELS (
    labelid         INT             PRIMARY KEY,
    labelname       VARCHAR2(30)    NOT NULL UNIQUE
);

CREATE TABLE "VIRDIRAPPTOR".EVENTS_LABELS (
    eventid         INT,
    labelid         INT,
    CONSTRAINT EVENTS_LABELS_PK PRIMARY KEY (eventid, labelid),
    CONSTRAINT FK_EVENTS_LABELS_EVENT FOREIGN KEY (eventid) REFERENCES EVENTS(eventid),
    CONSTRAINT FK_EVENTS_LABELS_LABEL FOREIGN KEY (labelid) REFERENCES LABELS(labelid)
);

CREATE TABLE "VIRDIRAPPTOR".FOLLOWS (
    personidone     INT     NOT NULL,
    personidtwo     INT     NOT NULL,
    CONSTRAINT FOLLOWS_PK PRIMARY KEY (personidone, personidtwo),
    CONSTRAINT FK_FOLLOWS_PERSONONE FOREIGN KEY (personidone) REFERENCES PERSONS(personid),
    CONSTRAINT FK_FOLLOWS_PERSONTWO FOREIGN KEY (personidtwo) REFERENCES PERSONS(personid)
);