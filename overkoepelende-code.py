from bs4 import BeautifulSoup
from pprint import pprint
import math
import subprocess
import re
import moviepy.editor as mpe
import os.path

def natural_sort_key(s, _nsre=re.compile('([0-9]+)')):
    return [int(text) if text.isdigit() else text.lower()
            for text in re.split(_nsre, s)]

class AudioClass:
    def __init__(self, audio_location, second_offset):
        self.audio_location = audio_location
        self.second_offset = second_offset

    def print_information(self):
        print "audio location:    ", self.audio_location
        print "offset in seconden:", self.second_offset


class VideoClass:
    def __init__(self, video_location, xml_location, frame_offset, fps=25):
        self.video_location = video_location
        self.xml_location = xml_location
        self._frame_offset = frame_offset
        self.fps = fps
        self.xml_structure = {}
        self.read_xml_camera()

    @property
    def frame_offset(self):
        return self._frame_offset

    @frame_offset.setter
    def frame_offset(self, frame_offset):
        self._frame_offset = frame_offset

    @property
    def second_offset(self):
        return self._frame_offset / float(self.fps)

    @second_offset.setter
    def second_offset(self, second_offset):
        self._frame_offset = int(second_offset * self.fps)

    def print_information(self):
        print "video location:    ", self.video_location
        print "xml location:      ", self.xml_location
        print "frames/second:     ", self.fps
        print "offset in frames:  ", self.frame_offset
        print "offset in seconden:", self.second_offset

    def read_xml_camera(self):
        soup = BeautifulSoup(open(self.xml_location), "xml")
        self.xml_structure = dict()
        self.xml_structure["cam_id"] = soup.camera.attrs["id"]
        self.xml_structure["shots"] = []
        self.xml_structure["persons"] = []
        self.xml_structure["actions"] = []

        for shot in soup.camera.findChildren("shot"):
            self.read_xml_shot(shot)

    def read_xml_shot(self, shot):
        shot_info = dict()
        shot_info["start_frame"] = shot.attrs["startframe"]
        shot_info["end_frame"] = shot.attrs["endframe"]
        shot_info["type"] = shot.attrs["type"]
        self.xml_structure["shots"].append(shot_info)
        for person in shot.findChildren("person"):
            self.read_xml_person(person)

    def read_xml_person(self, person):
        person_info = dict()
        person_info["person_id"] = person.attrs["id"]
        person_info["start_frame"] = person.findChildren("action")[0].attrs["startframe"]
        person_info["end_frame"] = person.findChildren("action")[-1].attrs["endframe"]
        self.xml_structure["persons"].append(person_info)
        for action in person.findChildren("action"):
            self.read_xml_action(action, person_info["person_id"])

    def read_xml_action(self, action, person_id):
        action_info = dict()
        action_info["start_frame"] = action.attrs["startframe"]
        action_info["end_frame"] = action.attrs["endframe"]
        try: action_info["type"] = action.speed.attrs["value"]
        except AttributeError: action_info["type"] = action.attrs["motion"]
        action_info["person_id"] = person_id
        self.xml_structure["actions"].append(action_info)


class Converter:
    def __init__(self,
                 seconds_per_pframe=0.4,
                 translation_dries_shot={"long": "ls", "ls":"ls", "np":"np","ms":"ms","No Person": "np", "Medium": "ms"},
                 pframes_per_pprogram=1000):
        self.translation_dries_shot = translation_dries_shot
        self.pframes_per_pprogram = pframes_per_pprogram
        self.seconds_per_pframe = seconds_per_pframe

    def translate_dries_shot(self, dries_shot):
        return self.translation_dries_shot[dries_shot]

    def frame2pframe(self, frame, video, round_ud=0):
        """
        :type video: VideoClass
        """
        return int((int(frame) - video.frame_offset) / (self.seconds_per_pframe * video.fps) +
                   ((int(frame) - video.frame_offset) % (
                       self.seconds_per_pframe * video.fps) - round_ud) / 10 + round_ud)

    @staticmethod
    def translate_dries_person(dries_person):
        return "pers" + str(dries_person)

    def __shots2string(self, video, structure, starts_of_sequence, ends_of_sequence, minimal_pframe,
                       maximal_pframe):
        output_string = ""
        for shot in structure["shots"]:
            start_frame = shot["start_frame"]
            end_frame = shot["end_frame"]
            start_pframe = self.frame2pframe(start_frame,
                                                  video,
                                                  1 if start_frame in starts_of_sequence else 0)
            end_pframe = self.frame2pframe(end_frame,
                                                video,
                                                -1 if end_frame in ends_of_sequence else 0)
            if start_pframe > end_pframe \
                    or start_pframe > maximal_pframe \
                    or end_pframe < minimal_pframe:
                continue

            output_string += "input_shot(" + \
                             str(start_pframe)
            output_string += "," + \
                             str(end_pframe)
            output_string += "," + structure["cam_id"] + "," + self.translate_dries_shot(shot["type"]) + ").\n"
        return output_string

    def __persons2string(self, video, structure, starts_of_sequence, ends_of_sequence, minimal_pframe,
                         maximal_pframe):
        output_string = ""

        for person in structure["persons"]:

            start_frame = person["start_frame"]
            end_frame = person["end_frame"]
            start_pframe = self.frame2pframe(start_frame,
                                                  video,
                                                  1 if start_frame in starts_of_sequence else 0)
            end_pframe = self.frame2pframe(end_frame,
                                                video,
                                                -1 if end_frame in ends_of_sequence else 0)

            if start_pframe > end_pframe \
                    or start_pframe > maximal_pframe \
                    or end_pframe < minimal_pframe:
                continue
            output_string += "input_person(" + \
                             str(start_pframe)

            output_string += "," + \
                             str(end_pframe)
            output_string += "," + structure["cam_id"] + "," + self.translate_dries_person(person["person_id"]) + ").\n"
        return output_string

    def __actions2string(self, video, structure, starts_of_sequence, ends_of_sequence, minimal_pframe,
                         maximal_pframe):
        output_string = ""
        for action in structure["actions"]:
            start_frame = action["start_frame"]
            end_frame = action["end_frame"]
            start_pframe = self.frame2pframe(start_frame,
                                                  video,
                                                  1 if start_frame in starts_of_sequence else 0)
            end_pframe = self.frame2pframe(end_frame,
                                                video,
                                                -1 if end_frame in ends_of_sequence else 0)
            if start_pframe > end_pframe \
                    or start_pframe > maximal_pframe \
                    or end_pframe < minimal_pframe:
                continue
            output_string += "input_action(" + str(start_pframe) + ","
            output_string += str(end_pframe) + ","
            output_string += structure["cam_id"] + ","
            output_string += self.translate_dries_person(action["person_id"]) + ","
            output_string += action["type"] + ").\n"
        return output_string

    def xml_struct2string(self, video, minimal_pframe, maximal_pframe):

        structure = video.xml_structure
        starts_of_sequence = [shot["start_frame"] for j, shot in enumerate(structure["shots"])
                              if int(shot["start_frame"]) != int(structure["shots"][j - 1]["end_frame"]) + 1]
        ends_of_sequence = [shot["end_frame"] for j, shot in enumerate(structure["shots"])
                            if j == len(structure["shots"]) - 1 or
                            int(shot["end_frame"]) != int(structure["shots"][j + 1]["start_frame"]) - 1]

        output_string = ""
        output_string += "cam(" + str(structure["cam_id"]) + ").\n"
        output_string += self.__shots2string(video,
                                             structure,
                                             starts_of_sequence,
                                             ends_of_sequence,
                                             minimal_pframe,
                                             maximal_pframe)
        output_string += self.__persons2string(video,
                                               structure,
                                               starts_of_sequence,
                                               ends_of_sequence,
                                               minimal_pframe,
                                               maximal_pframe)
        output_string += self.__actions2string(video,
                                               structure,
                                               starts_of_sequence,
                                               ends_of_sequence,
                                               minimal_pframe,
                                               maximal_pframe)
        return output_string


class EmptyException(Exception):
    pass


class ExecuteProblog:
    def __init__(self,
                 input_videos,
                 seconds_per_pframe=0.4,
                 pframes_per_pprogram=40,
                 second_start_of_fragment=700,
                 seconds_of_fragment=90,
                 problog_code="~/Documents/Masterproef_YentlStorms/original_problog_code.prob",
                 problog_command="~/Documents/Doctoraat/programs/problog2.1/problog-cli.py sample"):

        self.converter = Converter(pframes_per_pprogram=pframes_per_pprogram, seconds_per_pframe=seconds_per_pframe)
        self.videos = input_videos
        self.problog_code = problog_code
        self.problog_command = problog_command
        self.second_start_of_fragment = second_start_of_fragment
        self.seconds_per_pframe = seconds_per_pframe
        self.seconds_of_fragment = seconds_of_fragment
        self._pframes_per_pprogram = pframes_per_pprogram
        self.montage = []

    @property
    def pframes_per_pprogram(self):
        return self._pframes_per_pprogram

    @pframes_per_pprogram.setter
    def pframes_per_pprogram(self, pframes_per_pprogram):
        self._pframes_per_pprogram = int(pframes_per_pprogram)

    @property
    def seconds_per_pprogram(self):
        return self._pframes_per_pprogram * self.seconds_per_pframe

    @seconds_per_pprogram.setter
    def seconds_per_pprogram(self, seconds_per_pprogram):
        self._pframes_per_pprogram = int(seconds_per_pprogram / float(self.seconds_per_pframe))

    def get_montage_in_seconds(self):
        mis = list()
        for shot in self.montage:
            sshot = dict()
            sshot["camera"] = shot["camera"]
            sshot["start"] = shot["start_frame"] * self.seconds_per_pframe
            sshot["end"] = (shot["end_frame"]+0) * self.seconds_per_pframe
            mis.append(sshot)
        return mis

    def execute(self):
        max_index = int(math.ceil(self.seconds_of_fragment / (self.seconds_per_pframe * self.pframes_per_pprogram)))
        for i in range(0, max_index):
            try:
                problog_output = self.execute_single(i)
                shots = self.get_best_montage(problog_output)
                self.append_shots(shots)
                print "montage:", self.montage
            except EmptyException:
                break

    def get_best_montage(self, montages):
        probabilities = [float(montage.split(":")[-1]) for montage in montages.splitlines()]
        string_shots = [montage.split("%")[0].split(".") for montage in montages.replace(" ","").splitlines() if float(montage.split(":")[-1]) == max(probabilities)][0]
        string_shots.sort(key=natural_sort_key)
        string_shots = filter(lambda a: a != '', string_shots)
        string_shots = filter(lambda a: not "problem" in a, string_shots)
        if len(string_shots) == 0:
            raise EmptyException

        shots = []
        for ss in string_shots:
            string = ss
            string = string.replace("shot(","")
            string = string.replace(")","")
            components = string.split(",")
            shot = dict()
            shot["start_frame"] = int(components[0])
            shot["end_frame"] = int(components[1])
            shot["camera"] = components[2]
            shots.append(shot)
        return shots

    def append_shots(self, shots):
        try:
            if self.montage[-1]["camera"] == shots[0]["camera"]:
                self.montage[-1]["end_frame"] = shots.pop(0)["end_frame"]
        except IndexError:
            pass
        self.montage += shots

    def execute_single(self, index):
        minimal = int(index * self.pframes_per_pprogram + self.second_start_of_fragment / self.seconds_per_pframe) + 1
        maximal = int( (index + 1) * self.pframes_per_pprogram + self.second_start_of_fragment / self.seconds_per_pframe )

        problog_input = self.last_used()
        if problog_input == "":
            problog_input += "change_candidate(T,C):- lower_time_border(T), T1 is T+1, cam(C), possible_future(T1,C).\n"

        problog_input += "lower_time_border("+str(minimal-1)+").\n"
        problog_input += "time(T):-between("+str(minimal)+","+str(maximal)+",T).\n"

        for vid in self.videos:
            problog_input += self.converter.xml_struct2string(vid, minimal_pframe=minimal, maximal_pframe=maximal)

        print "\n\nproblog program number: ", index, "\n"
        print "input: \n-----\n", problog_input, "\n"

        filename = "tmp" + str(index) + ".prob"
        text_file = open(filename, "w")
        text_file.write(problog_input)
        text_file.close()

        arguments = "cat " + filename + " " + self.problog_code + " > tmp"
        print "$", arguments
        subprocess.call(arguments, shell=True)

        arguments = self.problog_command + " tmp -N 5 --with-probability --oneline"
        print "$", arguments
        pipe = subprocess.Popen(arguments, shell=True, stdout=subprocess.PIPE)
        c = pipe.stdout.read()
        print c
        return c

    def last_used(self):
        use_cams = ""
        try:
            for frame in range(self.montage[-1]["start_frame"],self.montage[-1]["end_frame"] + 1):
                i = str(frame)
                use_cams += "use_cam("+i+","+self.montage[-1]["camera"]+","+str(self.montage[-1]["start_frame"])+").\n"
        except IndexError:
            pass
        return use_cams


class VideoEditor:
    def __init__(self, videos, shots, audio_location, video_height=None, video_width=None):
        self.videos = videos
        self.shots = shots
        self.audio_location = audio_location
        self.video_height = video_height
        self.video_width = video_width

    def edit(self, with_sound=True):
        fragments = []
        if len(self.shots) == 0:
            print "No solution found !!!"
            return

        print "start editing"

        for shot in self.shots:
            video = self.get_video_by_id(shot["camera"])

            start_of_clip = shot["start"]+video.second_offset
            end_of_clip = shot["end"]+video.second_offset
            clip = mpe.VideoFileClip(video.video_location).subclip(start_of_clip,end_of_clip)

            clip = self.add_indicator(clip, shot["camera"])

            if self.video_width and self.video_height:
                clip.resize(newsize=(self.video_height,self.video_width))
            elif self.video_width:
                clip.resize(width=self.video_width)
            elif self.video_height:
                clip.resize(height=self.video_height)
            else:
                pass
            fragments.append(clip)

        print "clips start concatenation"
        final_video = mpe.concatenate_videoclips(fragments, method="compose")
        print "start writing video file"
        final_video.write_videofile("video_soundless.mp4", fps=25, preset="ultrafast")

        if with_sound:
            if not os.path.isfile("./audio.mp3"):
                print "creating audio file"
                self.video2audio(self.audio_location,"audio.mp3")

            print "adding sound to video"
            self.add_audio("audio.mp3",self.videos[0].second_offset,"video_soundless2.mp4", self.shots[0]["start"]+self.videos[0].second_offset,"final_video2.mp4")

    def get_video_by_id(self, id):
        for video in self.videos:
            if video.xml_structure["cam_id"] == id:
                return video
        raise EmptyException

    def add_indicator(self, video_clip, text):
        txt_clip = mpe.TextClip(text,fontsize=70,color='white')
        txt_clip = txt_clip.set_pos(("left","bottom")).set_duration(5)
        video = mpe.CompositeVideoClip([video_clip, txt_clip])
        return video

    def video2audio(self, video, audio):
        command = "avconv -y -i "+ video + " -vn " + audio
        print "$", command
        subprocess.call(command,shell=True)

    def add_audio(self, audio, audio_offset, video, video_offset, output):
        avoffset = audio_offset - video_offset
        if avoffset < 0:
            command = "avconv -y -itsoffset "+ str(-avoffset) + " -i " + video + " -i " + audio + " " + output
        else:
           command = "avconv -y -i " + video + " -itsoffset " + str(avoffset) + " -i " + audio + " " + output

        print "$", command
        subprocess.call(command, shell=True)


def TijdelijkeFunctie_camerause2xml():
    f = open("/media/nas/toptalk_vesalius/camera_use")
    output_string = '<camera id="cam0">\n'
    for line in f:
        if "---" in line:
            continue
        if not "camera: 0" in line:
            continue
        line = line.replace("camera: 0\t","")
        line = line.replace("\n","")

        output_string += '<shot startframe="'+line.split("->")[0]+'" endframe="'+line.split("->")[1]+'" type="No Person">'
        output_string += "<rest /> </shot>\n"
    output_string+="</camera>"
    print output_string


def main():
    videos = [VideoClass("/media/bae/cametron/Videos/TinneLecture/cam1Tinne/00080.MTS","/media/nas/tinne lessen/jay_cams/cam1Tinne/test2", frame_offset=1400),
              VideoClass("/media/nas/tinne lessen/test2.mp4","/media/nas/tinne lessen/Detections.xml", frame_offset=3432),
              VideoClass("/media/bae/cametron/Videos/TinneLecture/cam2Audience/00265.MTS","/media/nas/tinne lessen/jay_cams/cam2Audience/test2", frame_offset=1850)]



    #raw_input()
    #videos = [VideoClass("/media/nas/toptalk_vesalius/compressed_fine/montage.avi", "xml_camera0.xml", frame_offset=0),
    #          VideoClass("/media/nas/toptalk_vesalius/compressed_fine/cam1.avi", "xml_camera1.xml", frame_offset=3035),
    #          VideoClass("/media/nas/toptalk_vesalius/compressed_fine/cam2.avi", "xml_camera2.xml", frame_offset=2045)]

    problog_executor = ExecuteProblog(videos,
                 seconds_per_pframe=0.4,
                 pframes_per_pprogram=40,
                 second_start_of_fragment=200,
                 seconds_of_fragment=500)
    problog_executor.execute()
    montage = problog_executor.get_montage_in_seconds()
    print montage

    editor = VideoEditor(   videos,
                            montage,
                            audio_location="/media/bae/cametron/Videos/TinneLecture/cam1Tinne/00080.MTS",
                            video_height=460,
                            video_width=720)
    editor.edit(with_sound=True)


if __name__ == "__main__":
    main()
