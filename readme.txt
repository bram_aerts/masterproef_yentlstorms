Problog
=====

Deze link: "https://dtai.cs.kuleuven.be/problog/tutorial.html" wijst je naar de problog tutorial pagina, hier vind je een zeer duidelijke uitleg van hoe problog werkt. Je kan hier ook onder "interactive"->"editor" eens zelf een voorbeeldje uittesten.

Ik raad aan problog ook zelf te downloaden zodat je dit lokaal kan uitvoeren.
De voordelen hiervan zijn:
- je kan onbeperkt* grote programma's uitvoeren (op de server zit er een geheugen/tijdslimiet op)
		*je moet wel rekening houden met beschikbare RAM uiteraard
- gemakkelijker op te roepen uit andere programmeertalen (C/C++/python/...) mbv system commando's
- je kan sampling toepassen
Al deze zaken zijn nodig voor het uitvoeren van mijn code.
Op https://dtai.cs.kuleuven.be/problog/index.html#download vind je de link om problog te downloaden. (lees de readme in de problog folder voor details)
Ikzelf werk op linux(ubuntu), het is ook mogelijk onder windows te werken, maar daar heb ik geen ervaring mee. 


Het commando dat ik gebruik om mijn code uit te voeren is het volgende:
"pad van problogfolder"/problog-cli.py sample original_problog.prob -N 5
(met dit commando neem je 5 samples, degene met de hoogste waarschijnlijkheid is de beste)
"pad van problogfolder"/problog-cli.py sample original_problog.prob -N 5 --with-probability 
om deze waarschijnlijkheden ook te tonen.
het duurt enkele seconden voor de output verschijnt.



Bestanden
=====

original_problog.prob: problog programma met zowel input als code
original_problog_code.prob: problog zonder input (handig om zelf input bij te plaatsen)



Todo
=====

Dries: beeldverwerkingscode


Bram: overkoepelende code
- omzetten informatie gegenereerd door beeldverwerking naar problog input
- output van problog omzetten in een video








